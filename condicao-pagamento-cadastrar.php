<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

$ds_condicao = '';
$qtde_parcela = '';
$entrada = SEM_ENTRADA;
$situacao = CONDPGTO_ATIVO;
if ($_POST) {

  $entrada = isset($_POST['entrada']) ? $_POST['entrada'] : false;
  $ds_condicao = $_POST['ds_condicao'];
  $qtde_parcela = $_POST['qtde_parcela'];
  
      if ($entrada) {
        $entrada = COM_ENTRADA;
    } else {
        $entrada = SEM_ENTRADA;
    }
    
    if (empty($ds_condicao)) {
        $msg[] = 'Campo descrição esta vazio, insira uma descrição';
    }
    if (($entrada != COM_ENTRADA) and ( $entrada != SEM_ENTRADA)) {
        $msg[] = 'Campo entrada esta com valor inválido, selecione um valor válido';
    }
    if (empty($qtde_parcela) and ( $qtde_parcela != 0)) {
        $msg[] = 'Campo quantidade de parcelas esta vazio, digite quantidade';
    }
    if ($qtde_parcela < 0) {
        $msg[] = 'Campo quantidade de parcelas esta com valor inválido, digite valor válido';
    }
    
    if (!$msg) {
       
    $sql = "Insert Into condpgto(ds_condicao, qtde_parcela, situacao, entrada) Values('$ds_condicao','$qtde_parcela','$situacao','$entrada' '" . CONDPGTO_ATIVO . "')";

    $r = mysqli_query($con, $sql);

    if (!$r) {
    $msg[] = 'Erro para salvar';
    $msg[] = mysqli_error($con);
    }
    else {
    javascriptAlertFim('Registro salvo', 'condicao-pagamento.php');
  }
 }
}

?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cadastrar condição de pagamento</title>

    <?php headCss(); ?>
  </head>
  <body>

    <?php include 'nav.php'; ?>

    <div class="container">

      <div class="row">
        <div class="col-xs-12">
          <div class="page-header">
            <h1><i class="fa fa-cubes"></i> Cadastrar condição de pagamento</h1>
          </div>
        </div>
      </div>

      <?php
      if ($msg) {
          msgHtml($msg);
      }
      ?>

      <form class="row" role="form" method="post" action="condicao-pagamento-cadastrar.php">
        <div class="col-xs-12">

          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label for="fds_condicao">Descrição</label>
                <input type="text" class="form-control" id="fds_condicao" name="ds_condicao" placeholder="Descrição"
                value="<?php echo $ds_condicao; ?>">
              </div>
            </div>
          </div>
                      <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label for="fds_qtde_parcela">Número de parcelas</label>
                <input type="number" class="form-control" id="fqtde_parcela" name="qtde_parcela" placeholder="Número de parcela"
                value="<?php echo $qtde_parcela; ?>">
              </div>
            </div>
          </div>
                     <div class="row">
                        <div class="form-group">
                                      <div class="checkbox">
                            <label for="fativo">
                                <input type="checkbox" name="entrada" id="fativo"
                                       <?php if ($entrada == COM_ENTRADA) { ?> checked<?php } ?>
                                       > Entrada ativo
                            </label>
                        </div>
                                    <div class="help-block" >Deseja ter entrada na condição de pagamento, então clique acima.</div>
                                </div>  
                     </div>

          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary">Salvar</button>
              <button type="reset" class="btn btn-danger">Cancelar</button>
            </div>
          </div>
        </div>
      </form>

    </div>

    <script src="./lib/jquery.js"></script>
    <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
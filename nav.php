<?php
if ($_POST) {
    $nivelusu = 0;

    $sql = "Select * From usuario
  Where (nivelusu = '$nivelusu')
    And (situacao = '" . USUARIO_ATIVO . "')";

    require 'lib/conexao.php';
    $consulta = mysqli_query($con, $sql);
    $usuario = mysqli_fetch_assoc($consulta);

    if ($usuario) {
        session_start();
        $_SESSION['logado'] = 1;
        $_SESSION['idusuario'] = $usuario['idusuario'];
        $_SESSION['nome'] = $usuario['nome'];
        $_SESSION['nivelusu'] = $usuario['nivelusu'];
        print_r($_SESSION);
        exit;

        //header('location:index.php');
        //exit;
    }
}
?>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Tikara sistemas</a>
        </div>

        <div class="collapse navbar-collapse" id="nav1">
            <ul class="nav navbar-nav">
                <li><a href="./"><i class="fa fa-home"></i> Home</a></li>
                <li class="dropdown">
                    <?php if ($_SESSION['nivelusu'] == 1) { ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Pessoas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="clientes-cadastrar.php">Cadastrar Clientes</a></li>
                            <li><a href="clientes.php">Pesquisar clientes</a></li>
                            <li class="divider"></li>
                            <li><a href="usuarios-cadastrar.php">Cadastrar Usuários</a></li>
                            <li><a href="usuarios.php">Pesquisar Usuários</a></li>
                            <li class="divider"></li>
                            <li><a href="cidades-cadastrar.php">Cadastrar Cidades</a></li>
                            <li><a href="cidades.php">Pesquisar Cidades</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-barcode"></i> Produtos <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="produtos-cadastrar.php">Cadastrar produtos</a></li>
                            <li><a href="produtos.php">Pesquisar produtos</a></li>
                            <li class="divider"></li>
                            <li><a href="categorias-cadastrar.php">Cadastrar categorias</a></li>
                            <li><a href="categorias.php">Pesquisar categorias</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-truck"></i> Compras <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="compras.php">Pesquisar compras</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i> Vendas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="vendas.php">Pesquisar vendas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-money"></i> Lançamento <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="saldoinicial.php">Saldo Inicial</a></li>
                            <li><a href="caixa.php">Pagamento do caixa</a></li>
<!--                            <li class="divider"></li>
                            <li><a href="contas-pagar-cadastrar.php">Cadastrar contas a pagar</a></li>
                            <li><a href="contas-pagar.php">Pesquisar contas</a></li>-->
<!--                            <li class="divider"></li>
                            <li><a href="condicao-pagamento-cadastrar.php">Cadastrar condição de pagamento</a></li>
                            <li><a href="condicao-pagamento.php">Pesquisar condição de pagamento</a></li>-->
                         <li class="divider"></li>
<!--                            <li><a href="condicao-pagamento-cadastrar.php">Cadastrar condição de pagamento</a></li>-->
                            <li><a href="gerar-contas-parcela.php">Pesquisar parcelas geradas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list"></i> Relatórios <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="rel1.php">Compras por cliente</a></li>
                            <li><a href="rel2.php">Compras - Valor de venda x Valor pago</a></li>
                            <li><a href="rel3.php">Estoque de produto</a></li>
                        </ul>
                    </li>
                <?php } else { ?>

                <?php } if ($_SESSION['nivelusu'] == 2) { ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pessoas <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="clientes-cadastrar.php">Cadastrar Clientes</a></li>
                        <li><a href="clientes.php">Pesquisar clientes</a></li>
                        <!--                        <li class="divider"></li>
                                                <li><a href="usuarios-cadastrar.php">Cadastrar Usuários</a></li>
                                                <li><a href="usuarios.php">Pesquisar Usuários</a></li>-->
                        <li class="divider"></li>
                        <li><a href="cidades-cadastrar.php">Cadastrar Cidades</a></li>
                        <li><a href="cidades.php">Pesquisar Cidades</a></li>
                    </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produtos <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="produtos-cadastrar.php">Cadastrar produtos</a></li>
                            <li><a href="produtos.php">Pesquisar produtos</a></li>
                            <li class="divider"></li>
                            <li><a href="categorias-cadastrar.php">Cadastrar categorias</a></li>
                            <li><a href="categorias.php">Pesquisar categorias</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Encomendas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="encomendas.php">Pesquisar encomendas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="vendas.php">Pesquisar vendas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Relatórios <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="rel1.php">Compras por cliente</a></li>
                            <li><a href="rel2.php">Compras - Valor de venda x Valor pago</a></li>
                        </ul>
                    </li>
                <?php } ?>

                 
                <li><a href="logout.php"><i class="fa fa-close"></i> Sair</a></li>
            </ul>
        </div>
</nav>
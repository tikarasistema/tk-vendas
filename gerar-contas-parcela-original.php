<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
$msg = array();


//print_r($_SESSION);exit;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Contas a pagar</title>

        <?php headCss(); ?>
    </head>
    <body>
        <?php include 'nav.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-money"></i> Contas a pagar</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Informações das parcelas </h3>

                        </div>
                        <?php
                        $dtinicio = '';
                        if (isset($_GET['dtinicio'])) {
                            $dtinicio = trim($_GET['dtinicio']);
                        }
                        
                         $dtfinal = '';
                        if (isset($_GET['dtfinal'])) {
                            $dtfinal = trim($_GET['dtfinal']);
                        }
                        ?>

                        <form class="panel-body form-inline" role="form" method="get" action="">
                            <div class="form-group">
                            DATA INICIAL <input type="date" class="form-control" id="fdtinicio" name="dtinicio">
                            DATA FINAL <input type="date" class="form-control" id="fdtfinal" name="dtfinal">
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                            <button type="button" class="btn btn-danger" value="Limpar" onClick="resetForm();">Limpar</button>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover ">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Compra</th>
                                        <th class="text-center">Data Emissão</th>
                                        <th class="text-center">Data Vencimento</th>
                                        <th class="text-center">Situação / Dia(s)</th>
                                        <th class="text-center">Parcela</th>
                                        <th class="text-center">Valor Parcela</th>
                                        <th class="text-center">Valor Pago</th>
                                        <th class="text-center">Valor Pendente</th>
                                        <th class="text-center">Data Pagamento</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Pagar Parcela</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    
                                    //verifica a página atual caso seja informada na URL, senão atribui como 1ª página 
                                    if ((!$dtinicio)and(!$dtfinal)) {

                                        $pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
                                        //seleciona todos os itens da tabela 
                                        $cmd = "SELECT * FROM contaspagarparcelas WHERE idparcela = idparcela";
                                        $contas = mysqli_query($con, $cmd);
                                        //conta o total de itens 
                                        $total = mysqli_num_rows($contas);
                                        //seta a quantidade de itens por página, neste caso, 2 itens 
                                        $registros = 5;
                                        //calcula o número de páginas arredondando o resultado para cima 
                                        $numPaginas = ceil($total / $registros);
                                        //variavel para calcular o início da visualização com base na página atual 
                                        $inicio = ($registros * $pagina) - $registros;
                                        //seleciona os itens por página 
                                        $cmd = "SELECT * FROM contaspagarparcelas WHERE idparcela = idparcela limit $inicio,$registros";
                                        $contas = mysqli_query($con, $cmd);
                                        $total = mysqli_num_rows($contas);
                                        //exibe os produtos selecionados 
                                    } else {
                                       $sql = "SELECT  * FROM contaspagarparcelas where  vencimento_movimento between '$dtinicio' and '$dtfinal'";

                                        $contas = mysqli_query($con, $sql);
                                    }

                                    while ($conta = mysqli_fetch_assoc($contas)) {
                                        //pega a soma da parcela amortizacao e subtrai com o valor da parcela
                                        $total_pago = 0;
                                        $vlr_parcela = 0;
                                        $total_pago = $conta['total_pago'];
                                        $vlr_parcela = $conta['valor_movimento'];

                                        $difrestante = $vlr_parcela - $total_pago;

                                        if ($conta['situacao_parcela'] == PARCELA_ABERTA) {
                                            $bgcolor = "#FAEBD7";
                                        } else if ($conta['situacao_parcela'] == PARCELA_BAIXADA) {
                                            $bgcolor = "#E0FFFF";
                                        } else if($conta['situacao_parcela'] == PARCELA_BAIXA_PARCIAL){
                                            $bgcolor = "#FFE4C4";
                                        } else{
                                           $bgcolor = "#BFEFFF"; 
                                        }

                                        $data2 = $conta['vencimento_movimento'];
                                        $datahoje = date('Y-m-d');
                                        //$data1 = $conta['data_movimento']; 

                                        $time_atual = strtotime($datahoje);
                                        $time_expira = strtotime($data2);
                                        $dif_tempo = $time_expira - $time_atual;
                                        $dias = (int) floor($dif_tempo / (60 * 60 * 24));
                                        ?>
                                        <tr>
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo $conta['idparcela']; ?></td>
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo $conta['idmovimento']; ?></td>
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo date('d/m/Y', strtotime($conta['data_movimento'])); ?></td>
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo date('d/m/Y', strtotime($conta['vencimento_movimento'])); ?></td>
    <!--                                        <td class="text-center" bgcolor="<?php //echo $bgcolor;  ?>"><?php //echo round($dataFinal,0);  ?></td>-->
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>">
                                                <?php if ($dias <= 30 && $dias > 0) { ?>
                                                    <font color="#FFA500">A VENCER (<?php echo $dias; ?>)</font>
                                                <?php } else if ($dias < 0) { ?>
                                                    <font color="#FF0000">VENCIDO (<?php echo $dias; ?>)</font>
                                                <?php } else { ?>
                                                    <font color="#32CD32">VIGENTE (<?php echo $dias; ?>)</font>
                                                <?php } ?> 
                                            </td>   
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo $conta['numero_parcela']; ?></td>
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo number_format($conta['valor_movimento'],2,",","."); ?></td>
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo number_format ($conta['total_pago'],2,",","."); ?></td>    
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo number_format($difrestante, 2,",", "."); ?></td>
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>"><?php echo date('d/m/Y', strtotime($conta['data_pagamento_parcela'])); ?></td>

                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>">
                                                <?php if ($conta['situacao_parcela'] == PARCELA_ABERTA) { ?>
                                                    <span class="label label-success">ABERTO</span>
                                                <?php } else if ($conta['situacao_parcela'] == PARCELA_BAIXADA) { ?>
                                                    <span class="label label-info">BAIXADA </span>
                                                <?php } else { ?>
                                                    <span class="label label-warning">BAIXA PARCIAL</span>
                                                <?php } ?>
                                            </td>
                                            
                                            <td class="text-center" bgcolor="<?php echo $bgcolor; ?>">                                              
                                            <a href="pagar-parcela.php?idparcela=<?php echo $conta['idparcela']; ?>&idcompra=<?php echo $conta['idmovimento']; ?>" title="Pagar parcela"><i class="fa fa-list-alt"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <p>Página:</p>
                    <?php
//exibe a paginação
                    if ((!$dtinicio)and(!$dtfinal)) {
                        if ($pagina > 1) {
                            echo "<a href='gerar-contas-parcela.php?pagina=" . ($pagina - 1) . "' class='controle'>&laquo; anterior</a>";
                        }

                        for ($i = 1; $i < $numPaginas + 1; $i++) {
                            $ativo = ($i == $pagina) ? 'numativo' : '';
                            echo "<a href='gerar-contas-parcela.php?pagina=" . $i . "' class='numero " . $ativo . "'> " . $i . " </a>";
                        }
                        if ($pagina < $numPaginas) {
                            echo "<a href='gerar-contas-parcela.php?pagina=" . ($pagina + 1) . "' class='controle'>proximo &raquo;</a>";
                        }
                    }
                    ?>
                </div>
                    <div class="col-xs-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Legenda</strong>
            </div>            
            <div class="panel-body">
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #FAEBD7"></i> <strong>Parcela Aberta</strong><br>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #d2eafd"></i> <strong>Parcela paga parcialmente</strong>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color:#ffd699;"></i> <strong>Vencido entre 1  e 29 dias</strong><br>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #f7a6a4"></i> <strong>Vencido à mais de 30 dias</strong>
            </div>
        </div>
    </div>
            </div>

        </div>
        <script src="./js/gerar-contas-parcela.js"></script>
        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/gerar-contas-parcela.css"/>
    </body>
</html>
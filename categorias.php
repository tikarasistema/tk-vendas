<?php
require './protege.php';
require './config.php';
require './lib/conexao.php';
require './lib/funcoes.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Categorias</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-cubes"></i> Categorias</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Categorias</h3>
                        </div>

                        <?php
                        $q = '';
                        if (isset($_GET['q'])) {
                            $q = trim($_GET['q']);
                        }
                        ?>
                        <form class="panel-body form-inline" role="form" method="get" action="">
                            <div class="form-group">
                                <label class="sr-only" for="fq">Pesquisa</label>
                                <input type="search" class="form-control" id="fq" name="q" placeholder="Pesquisa" value="<?php echo $q; ?>">
                            </div>
                            <button type="submit" class="btn btn-default">Pesquisar</button>
                        </form>

                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th></th>
                                    <th>Categoria</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>                           
                                <?php
                                //verifica a página atual caso seja informada na URL, senão atribui como 1ª página 
                                if (!$q) {

                                    $pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
                                    //seleciona todos os itens da tabela 
                                    $cmd = "select * from categoria ";
                                    $categorias = mysqli_query($con, $cmd);
                                    //conta o total de itens 
                                    $total = mysqli_num_rows($categorias);
                                    //seta a quantidade de itens por página, neste caso, 2 itens 
                                    $registros = 5;
                                    //calcula o número de páginas arredondando o resultado para cima 
                                    $numPaginas = ceil($total / $registros);
                                    //variavel para calcular o início da visualização com base na página atual 
                                    $inicio = ($registros * $pagina) - $registros;
                                    //seleciona os itens por página 
                                    $cmd = "select * from categoria limit $inicio,$registros";
                                    $categorias = mysqli_query($con, $cmd);
                                    $total = mysqli_num_rows($categorias);
                                    //exibe os produtos selecionados 
                                } else {

                                    $sql = "SELECT idcategoria, categoria, situacao FROM categoria";
                                    if ($q != '') {
                                       $sql .= " Where (categoria like '%$q%')or (idcategoria like '%$q%')";
                                    }
                                    $categorias = mysqli_query($con, $sql);
                                }
                                while ($categoria = mysqli_fetch_assoc($categorias)) {
                                    ?>

                                    <tr>
                                        <td><?php echo $categoria['idcategoria']; ?></td>
                                        <td>
                                            <?php if ($categoria['situacao'] == CATEGORIA_ATIVO) { ?>
                                                <span class="label label-success">ativo</span>
                                            <?php } else { ?>
                                                <span class="label label-warning">inativo</span>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $categoria['categoria']; ?></td>
                                        <td>
                                            <a href="categorias-editar.php?idcategoria=<?php echo $categoria['idcategoria']; ?>" title="Editar"><i class="fa fa-edit fa-lg"></i></a>
                                            <a href="categorias-apagar.php?idcategoria=<?php echo $categoria['idcategoria']; ?>" title="Remover"><i class="fa fa-times fa-lg"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <p>Página:</p>
                    <?php
                    //exibe a paginação
                    if (!$q) {
                        if ($pagina > 1) {
                            echo "<a href='categorias.php?pagina=" . ($pagina - 1) . "' class='controle'>&laquo; anterior</a>";
                        }

                        for ($i = 1; $i < $numPaginas + 1; $i++) {
                            $ativo = ($i == $pagina) ? 'numativo' : '';
                            echo "<a href='categorias.php?pagina=" . $i . "' class='numero " . $ativo . "'> " . $i . " </a>";
                        }
                        if ($pagina < $numPaginas) {
                            echo "<a href='categorias.php?pagina=" . ($pagina + 1) . "' class='controle'>proximo &raquo;</a>";
                        }
                    }
                    ?>
                </div>
            </div>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/categorias.css"/>

    </body>
</html>
<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

if (isset($_POST['idparcela'])) {
    $idparcela = (int) $_POST['idparcela'];
} else {
    $idparcela = (isset($_GET['idparcela']) ? $_GET['idparcela'] : null);
}

if (isset($_POST['idcompra'])) {
    $idcompra = (int) $_POST['idcompra'];
} else {
    $idcompra = (isset($_GET['idcompra']) ? $_GET['idcompra'] : null);
}

//verifica parcela vencida
$sql = "Select idparcela, data_movimento, vencimento_movimento, pagamento_movimento, valor_movimento, numero_parcela, situacao_parcela,data_pagamento_parcela From contaspagarparcelas Where idparcela = $idparcela";
$resultado = mysqli_query($con, $sql);
$registro = mysqli_fetch_assoc($resultado);
$dvenci = $registro['vencimento_movimento'];
$datahj = date('Y-m-d');

if (!$registro) {
    javascriptAlertFim('Parcela inexistente.', 'gerar-contas-parcela.php');
} else {

    $time_atual = strtotime($datahj);
    $time_expira = strtotime($dvenci);
    $dif_tempo = $time_expira - $time_atual;
    $dias = (int) floor($dif_tempo / (60 * 60 * 24));
    // verifica se a data está vencida e faz update para colocar o status como vencido
    if ($dias < 0) {
        $situacao_parcela = PARCELA_VENCIDA;
        $sql = "UPDATE contaspagarparcelas SET situacao_parcela='$situacao_parcela' WHERE idparcela = $idparcela";
        $update1 = mysqli_query($con, $sql);
        if (!$update1) {
            $msg[] = 'Falha ao atualizar a parcela ';
            $msg[] = mysqli_error($con);
            $msg[] = $sql;
        } else {
            javascriptAlertFim('Atenção ! \n Esta parcela venceu já faz: ' . $dias . 'dia(s)', 'gerar-contas-parcela.php');
        }
    }
}

// verifica se o valor a pagar e valor pago estão iguais
$sql = "Select idmovimento, idparcela,vencimento_movimento,pagamento_movimento, valor_movimento From contaspagarparcelas Where idparcela = $idparcela";
$resultado1 = mysqli_query($con, $sql);
$registro1 = mysqli_fetch_assoc($resultado1);
$valor_movimento = $registro1['valor_movimento'];
$pagamento_movimento = $registro1['pagamento_movimento'];

if (!$registro1) {
    javascriptAlertFim('Parcela inexistente', 'gerar-contas-parcela.php');
} else {
    if ($pagamento_movimento == $valor_movimento) { 
        $situacao_parcela = PARCELA_BAIXADA;
        $sql = "UPDATE contaspagarparcelas SET situacao_parcela='$situacao_parcela' WHERE idparcela = $idparcela";
        $update2 = mysqli_query($con, $sql);
        if (!$update2) {
            $msg[] = 'Falha ao dar baixa na parcela';
            $msg[] = mysqli_error($con);
            $msg[] = $sql;
        } else {
            javascriptAlertFim('Baixa parcial da parcela foi alterada com sucesso', 'gerar-contas-parcela.php');
        }
    }
}


if ($_POST) {
    $dinheiro = $_POST['dinheiro'];

    $pagamento_movimento = (float) $_POST['pagamento_movimento'];
    //$data_pagamento_parcela = $_POST['data_pagamento_parcela'];
    //$data_pagamento_parcela = date_converter($data_pagamento_parcela);
    $situacao_parcela = $_POST['ativo'];
    $datahoje = date('Y-m-d');
    $vencimento_movimento = $registro1['vencimento_movimento'];

    if ($vencimento_movimento < $datahoje) {
        $msg[] = 'O valor do pagamento não pode ser maior ao valor pago na parcela ';
    }    
    if ($pagamento_movimento < 0) {
        $msg[] = 'O valor não pode ser negativo';
    }
    if ($situacao_parcela == '') {
        $msg[] = 'Informe a situação da parcela';
    }

    if (!$msg) {
          
        $sql = "UPDATE contaspagarparcelas SET "
                . "pagamento_movimento='$pagamento_movimento',situacao_parcela='$situacao_parcela',data_pagamento_parcela='$datahoje' WHERE idparcela = $idparcela";

        $update = mysqli_query($con, $sql);
        if (!$update) {
            $msg[] = 'Falha ao alterar a baixa da parcela ';
            $msg[] = mysqli_error($con);
            $msg[] = $sql;
        } else {

            $sql = "Select idmovimento, vencimento_movimento,pagamento_movimento, valor_movimento, data_pagamento_parcela From contaspagarparcelas Where idparcela = $idparcela";
            $res = mysqli_query($con, $sql);
            $res = mysqli_fetch_assoc($res);
            $dt_pagto = date('Y-m-d H:i:s');
            $vlr_pago = $res['pagamento_movimento'];
            $vlr_movimento = $res['valor_movimento'];
            $idcompra = $res['idmovimento'];
            $tipo = 1;
            $idusuario = $_SESSION['idusuario'];
            //javascriptAlertFim('Gerado a baixa parcial da parcela no valor de R$ '.$pagamento_movimento.'', 'gerar-contas-parcela.php');

            $sql = "Insert into amortizacao_pagar (idparcela, dt_pagto, vlr_pago, idusuario, idcompra, tipo) values ('$idparcela', '$dt_pagto', '$vlr_pago', '$idusuario', '$idcompra', '$tipo'"
                    . ")";
            $insert = mysqli_query($con, $sql);
            if (!$insert) {
                $msg[] = 'Falha ao inserir a baixa da parcela ';
                $msg[] = mysqli_error($con);
                $msg[] = $sql;
              
            } else {
  
                $sql = "SELECT sum(vlr_pago) as vlr_pago FROM amortizacao_pagar where idparcela =' $idparcela'";
                $res2 = mysqli_query($con, $sql);
                $reg2 = mysqli_fetch_assoc($res2);
                $vlrPago = $reg2['vlr_pago'];
                   
                $sql = "UPDATE contaspagarparcelas SET "
                        . "pagamento_movimento='$vlrPago' WHERE idparcela = $idparcela";

                $atualiza = mysqli_query($con, $sql);
                if (!$atualiza) {
                    $msg[] = 'Falha ao alterar o pagamento parcial ';
                    $msg[] = mysqli_error($con);
                    $msg[] = $sql;
                }else{
                    //header('location: gerar-contas-parcela.php');
                     javascriptAlertFim('Valor pago de R$: '.$vlr_pago.' na parcela', 'gerar-contas-parcela.php');
                }
           
            }
            
        }
    }
} else {

    $pagamento_movimento = $registro['pagamento_movimento'];
    $situacao_parcela = $registro['situacao_parcela'];
    $data = $registro['data_pagamento_parcela'];
    //print_r($data);exit;
    //$data1 = date("Y-m-d", strtotime(str_replace('/', '-', $data)));
    //$data2 = implode('/', array_reverse(explode('-', $data1)));
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Baixa na parcela</title>

<?php headCss(); ?>
    </head>
    <body>

<?php include 'nav.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-file-powerpoint-o"></i> Baixa naparcela #<?php echo $idparcela; ?></h1>
                    </div>
                </div>
            </div>

<?php
if ($msg) {
    msgHtml($msg);
}
?>

            <form class="row" role="form" method="post" action="pagar-parcela.php">
                <input type="hidden" name="idparcela" value="<?php echo $idparcela; ?>">
                <div class="row"> 
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            <label for="fpagamento_movimento">Valor R$</label>
                            <div class="input-group">
                                <span class="input-group-addon">R$</span>
                                <input type="text" class="form-control" id="fpagamento_movimento" name="pagamento_movimento" placeholder="Valor" value="<?php echo number_format($pagamento_movimento, 2, '.', ''); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            <label for="fativo">Situação</label>
                            <select class="form-control" name="ativo" id="fativo" required="">
                                <option value="">Selecione</option>
                                <option value="A" <?php echo $situacao_parcela == PARCELA_ABERTA ? 'selected' : ''; ?>>ABERTO</option>
                                <option value="B" <?php echo $situacao_parcela == PARCELA_BAIXADA ? 'selected' : ''; ?>>BAIXADA</option>
                            </select>
                        </div> 
                    </div>
                            
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="dinheiro" name="dinheiro" value="1">Dinheiro</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                

                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">Histórico na baixa da parcela # <?php echo $idparcela; ?></div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Compra</th>
                            <th class="text-center">Parcela</th>
                            <th class="text-center">Data Pago</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Valor Pago</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
<?php
$sql = "Select tipo,idparcela,dt_pagto,vlr_pago,idusuario,idcompra,idamortizacao from amortizacao_pagar where idparcela = $idparcela and idcompra= $idcompra";
$consulta3 = mysqli_query($con, $sql);
while ($resultado3 = mysqli_fetch_assoc($consulta3)) {
    ?>
                            <tr>
                                <td class="text-center"><?php echo $resultado3['idamortizacao']; ?></td>
                                <td class="text-center"><?php echo $resultado3['idcompra']; ?></td>
                                <td class="text-center"><?php echo $resultado3['idparcela']; ?></td>
                                <td class="text-center"><?php echo date('d/m/Y', strtotime($resultado3['dt_pagto'])); ?></td>
                                <td class="text-center"><?php
                        switch ($resultado3['tipo']) {
                            case AMORTIZACAO_BAIXA:
                                echo 'Baixa em dinheiro';
                                break;
                            case AMORTIZACAO_ESTORNO:
                                echo 'Estorno';
                                break;
                        }
    ?></td>
                                <td class="text-center">R$ <?php echo number_format($resultado3['vlr_pago'], 2, ",", "."); ?></td>
                                <td></td>
                            </tr><?php
                            }
?>
                    </tbody>
                </table>
            <div class="panel-footer">
     
            </div>
            </div>
        </div>
       
    </div>
    <script src="./js/pagar-parcela.js"></script>
    <script src="./lib/jquery.js"></script>
    <script src="./lib/mask.min.js"></script>
    <script src="./lib/jquery-3.2.1.min.js"></script>
    <script src="./lib/jquery.maskMoney.js"></script>
    <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
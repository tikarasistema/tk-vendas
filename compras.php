<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
$q = '';
if (isset($_GET['q'])) {
    $q = trim($_GET['q']);
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Compras</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="page-header">
                <h1><i class="fa fa-truck"></i> Compras</h1>
            </div>
            <?php
            $q = '';
            if (isset($_GET['q'])) {
                $q = trim($_GET['q']);
            }
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Compras</h3>
                </div>
                <div class="panel-body">
                    <form class="form-inline" role="form" method="get" action="">
                        <div class="form-group">
                            <label class="sr-only" for="fq">Pesquisa</label>
                            <input type="search" class="form-control" id="fq" name="q" placeholder="Pesquisa" value="<?php echo $q; ?>">
                        </div>
                        <button type="submit" class="btn btn-default">Pesquisar</button>
                    </form>
                </div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Status</th>
                            <th>Data</th>
                            <th>Nome/Razão social</th>
                            <th>Cpf/Cnpj</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        //verifica a página atual caso seja informada na URL, senão atribui como 1ª página 
                        if (!$q) {

                            $pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
                            //seleciona todos os itens da tabela 
                            $cmd = "Select
                                    e.idcompra,
                                    e.data,
                                    c.nome clienteNome,
                                    c.cpfcnpj,
                                    c.rgie,
                                    c.inTipo,
                                    c.celular,
                                    e.situacao compraStatus,
                                    (Select Sum(ei.precopago * ei.qtd) From compraitem ei Where (ei.idcompra = e.idcompra)) precoTotal
                                    From compra e Inner Join cliente c
                                    On (c.idcliente = e.idcliente)Inner Join usuario u
                                    On (u.idusuario = e.idusuario)";
                            $compras = mysqli_query($con, $cmd);
                            //conta o total de itens 
                            $total = mysqli_num_rows($compras);

                            //seta a quantidade de itens por página, neste caso, 2 itens 
                            $registros = 5;
                            //calcula o número de páginas arredondando o resultado para cima 
                            $numPaginas = ceil($total / $registros);
                            //variavel para calcular o início da visualização com base na página atual 
                            $inicio = ($registros * $pagina) - $registros;
                            //seleciona os itens por página 
                            $cmd = "Select
                                    e.idcompra,
                                    e.data,
                                    c.nome clienteNome,
                                    c.cpfcnpj,
                                    c.rgie,
                                    c.inTipo,
                                    c.celular,
                                    e.situacao compraStatus,
                                    (Select Sum(ei.precopago * ei.qtd) From compraitem ei Where (ei.idcompra = e.idcompra)) precoTotal
                                    From compra e Inner Join cliente c
                                    On (c.idcliente = e.idcliente)Inner Join usuario u
                                    On (u.idusuario = e.idusuario)limit $inicio,$registros";
                            $compras = mysqli_query($con, $cmd);
                            $total = mysqli_num_rows($compras);

                            //exibe os produtos selecionados 
                        } else {
                            $sql = "Select
                                    e.idcompra,
                                    e.data,
                                    c.nome clienteNome,
                                    c.cpfcnpj,
                                    c.rgie,
                                    c.inTipo,
                                    c.celular,
                                    e.situacao compraStatus,
                                    (Select Sum(ei.precopago * ei.qtd) From compraitem ei Where (ei.idcompra = e.idcompra)) precoTotal
                                    From compra e Inner Join cliente c
                                    On (c.idcliente = e.idcliente)Inner Join usuario u
                                    On (u.idusuario = e.idusuario)";
                            if ($q != '') {
                                $sql .= " Where (c.nome like '%$q%')or (c.cpfcnpj like '%$q%')";
                            }

                            $compras = mysqli_query($con, $sql);
                        }
                        while ($resultado = mysqli_fetch_assoc($compras)) {
                            //print_r($resultado);exit;
                            $compraData = strtotime($resultado['data']);
                            ?>                                           
                            <tr>
                                <td><?php echo $resultado['idcompra']; ?></td>
                                <td>
                                    <?php if ($resultado['compraStatus'] == COMPRA_FECHADA) { ?>
                                        <span class="label label-success">fechada</span>
                                    <?php } else { ?>
                                        <span class="label label-warning">aberta</span>
                                    <?php } ?>
                                </td>
                                <td><?php echo date('d/m/Y', $compraData); ?></td>
                                <td><?php echo $resultado['clienteNome']; ?></td>
                                <td><?php echo $resultado['cpfcnpj']; ?></td>
                                <td>R$ <?php echo number_format($resultado['precoTotal'], 2, ",", "."); ?></td>
                                <td>
                                    <?php if ($resultado['compraStatus'] == COMPRA_FECHADA) { ?>
                                        <a href="compra-detalhes.php?idcompra=<?php echo $resultado['idcompra']; ?>" title="Detalhes da encomenda"><i class="fa fa-align-justify fa-lg"></i></a>
                                    <?php } else { ?>
                                        <a href="compra-continuar.php?idcompra=<?php echo $resultado['idcompra']; ?>" title="Continuar encomenda"><i class="fa fa-play fa-lg"></i></a>
                                        <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <p>Página:</p>
            <?php
            //exibe a paginação
                 if (!$q) {
                        if ($pagina > 1) {
                            echo "<a href='compras.php?pagina=" . ($pagina - 1) . "' class='controle'>&laquo; anterior</a>";
                        }

                        for ($i = 1; $i < $numPaginas + 1; $i++) {
                            $ativo = ($i == $pagina) ? 'numativo' : '';
                            echo "<a href='compras.php?pagina=" . $i . "' class='numero " . $ativo . "'> " . $i . " </a>";
                        }
                        if ($pagina < $numPaginas) {
                            echo "<a href='compras.php?pagina=" . ($pagina + 1) . "' class='controle'>proximo &raquo;</a>";
                        }
                    }
            ?>
        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/compras.css"/>
    </body>
</html>
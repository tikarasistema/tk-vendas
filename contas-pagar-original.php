<?php
require './protege.php';
require './config.php';
require './lib/conexao.php';
require './lib/funcoes.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Contas a pagar</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-money"></i> Contas a pagar</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Listagem de contas a pagar</h3>
                        </div>
                        <?php
                        $q = '';
                        if (isset($_GET['q'])) {
                            $q = trim($_GET['q']);
                        }
                        ?>

                        <form class="panel-body form-inline" role="form" method="get" action="">
                            <div class="form-group">
                                <label class="sr-only" for="fq">Pesquisa</label>
                                <input type="search" class="form-control" id="fq" name="q" placeholder="Pesquisa" value="<?php echo $q; ?>">
                            </div>
                            <button type="submit" class="btn btn-default">Pesquisar</button>
                        </form>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome/razão social</th>
                                    <th>D.operação</th>
                                    <th>Descrição</th>
                                    <th>Total R$</th>
                                     <th>D.vencimento</th>
                                     <th>Total parcela</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                    <?php
                                //verifica a página atual caso seja informada na URL, senão atribui como 1ª página 
                        if (!$q) {
                            
                        $pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
                        //seleciona todos os itens da tabela 
                        $cmd = "SELECT cp.*, c.* FROM contaspagar cp inner join cliente c on cp.idcliente = c.idcliente WHERE idpagar = idpagar";
                        $contas = mysqli_query($con, $cmd);
                        //conta o total de itens 
                        $total = mysqli_num_rows($contas);
                        //seta a quantidade de itens por página, neste caso, 2 itens 
                        $registros = 5;
                        //calcula o número de páginas arredondando o resultado para cima 
                        $numPaginas = ceil($total / $registros);
                        //variavel para calcular o início da visualização com base na página atual 
                        $inicio = ($registros * $pagina) - $registros;
                        //seleciona os itens por página 
                        $cmd = "SELECT cp.*, c.* FROM contaspagar cp inner join cliente c on cp.idcliente = c.idcliente WHERE idpagar = idpagar limit $inicio,$registros";
                        $contas = mysqli_query($con, $cmd);
                        $total = mysqli_num_rows($contas);
                        //exibe os produtos selecionados 
                    } else {
                        $sql = "SELECT cp.*, c.* FROM contaspagar cp inner join cliente c on cp.idcliente = c.idcliente where idpagar = idpagar";
           
                        if ($q != '') {
                            
                            $sql .= " Where (descricao like '%$q%') or (nome like '%$q%')";
        
                        }
                        $contas = mysqli_query($con, $sql);
                       
                    }
                    while ($conta = mysqli_fetch_assoc($contas)) {
                     
                        $dataVenci = strtotime($conta['data_paga']);
                        $dataOperacao = strtotime($conta['data_operacao']);
                        ?>
                                    <tr>
                                       
                                        <td><?php echo $conta['idpagar']; ?></td>
                                        <td><?php echo $conta['nome']; ?></td>
                                        <td><?php echo date('d/m/Y', $dataOperacao); ?></td>
                                        <td><?php echo $conta['descricao']; ?></td>
                                        <td><?php echo $conta['valor']; ?></td>
                                        <td><?php echo date('d/m/Y', $dataVenci); ?></td>
                                        <td><?php echo $conta['num_parcela']; ?></td>
                                        <td>
                                            <a href="contas-pagar-editar.php?idpagar=<?php echo $conta['idpagar']; ?>" title="Editar"><i class="fa fa-edit fa-lg"></i></a>
<!--                                            <a href="contas-pagar-apagar.php?idpagar=<?php echo $conta['idpagar']; ?>" title="Remover"><i class="fa fa-times fa-lg"></i></a>-->
                                            <a href="gerar-contas.php?idpagar=<?php echo $conta['idpagar']; ?>&acao=1" title="Gerar parcelas"><i class="fa fa-money fa-lg"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <p>Página:</p>
                    <?php
                    //exibe a paginação
                 if (!$q) {
                        if ($pagina > 1) {
                            echo "<a href='contas-pagar.php?pagina=" . ($pagina - 1) . "' class='controle'>&laquo; anterior</a>";
                        }

                        for ($i = 1; $i < $numPaginas + 1; $i++) {
                            $ativo = ($i == $pagina) ? 'numativo' : '';
                            echo "<a href='contas-pagar.php?pagina=" . $i . "' class='numero " . $ativo . "'> " . $i . " </a>";
                        }
                        if ($pagina < $numPaginas) {
                            echo "<a href='contas-pagar.php?pagina=" . ($pagina + 1) . "' class='controle'>proximo &raquo;</a>";
                        }
                    }
                    ?>
                </div>
            </div>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/contas-pagar.css"/>
    </body>
</html>
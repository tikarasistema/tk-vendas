<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
$msgOk = array();
$msgAviso = array();
if (!isset($_GET['idvenda'])) {
    header('location:vendas.php');
    exit;
}
$idvenda = (int) $_GET['idvenda'];
$sql = "Select
	v.idvenda,
	v.data,
	c.nome clienteNome,
        c.cpfcnpj,
        c.rgie,
        c.telefone,
        c.endereco,
        c.cep,
        c.bairro,
        c.numero,
        c.celular,
        c.inTipo,
        c.email,
	u.nome usuarioNome
        From venda v
        Inner Join cliente c
	On (c.idcliente = v.idcliente)
        Inner Join usuario u
	On (u.idusuario = v.idusuario)
        Where
        (v.idvenda = $idvenda)
        And (v.situacao = " . VENDA_FECHADA . ")";
$consulta = mysqli_query($con, $sql);
$venda = mysqli_fetch_assoc($consulta);
if (!$venda) {
    header('location:vendas.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Detalhes da venda</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="page-header">
                <h1><i class="fa fa-shopping-cart"></i> Detalhes da venda #<?php echo $idvenda; ?></h1>
            </div>

            <?php
            if ($msgOk) {
                msgHtml($msgOk, 'success');
            }
            ?>
            <?php
            if ($msgAviso) {
                msgHtml($msgAviso, 'warning');
            }
            ?>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Produtos da venda</h3>
                </div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Qtd.</th>
                            <th>Produto</th>
                            <th>Preço unitário</th>
                            <th>Preço total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "Select
	v.idproduto,
	p.produto,
	v.precopago,
	v.qtd
From vendaitem v
Inner Join produto p
	On (p.idproduto = v.idproduto)
Where (v.idvenda = $idvenda)";
                        $consulta = mysqli_query($con, $sql);

                        $vendaTotal = 0;

                        while ($produto = mysqli_fetch_assoc($consulta)) {
                            $total = $produto['qtd'] * $produto['precopago'];
                            $vendaTotal += $total;
                            ?>
                            <tr>
                                <td><?php echo $produto['qtd']; ?></td>
                                <td><?php echo $produto['produto']; ?></td>
                                <td>R$ <?php echo number_format($produto['precopago'], 2, ',', '.'); ?></td>
                                <td>R$ <?php echo number_format($total, 2, ',', '.'); ?></td>
                            </tr>
<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th colspan="2">Total da venda</th>
                            <th>R$ <?php echo number_format($vendaTotal, 2, ',', '.'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <form class="form-horizontal" method="post" action="venda-fechar.php">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fechamento da venda</h3>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label for="fidvenda" class="col-sm-2 control-label">Código:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $idvenda; ?></p>
                            </div>

                            <label for="fdata" class="col-sm-2 control-label">Data:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo date('d/m/Y', strtotime($venda['data'])); ?></p>
                            </div>

                            <label for="ftotal" class="col-sm-2 control-label">Total:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static">R$ <?php echo number_format($vendaTotal, 2, ',', '.'); ?></p>
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="fcliente" class="col-sm-2 control-label">Cliente:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['clienteNome']; ?></p>
                            </div>
                            <label for="fcpf" class="col-sm-2 control-label">Cpf/Cnpj:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['cpfcnpj']; ?></p>
                            </div>
                             <label for="frgie" class="col-sm-2 control-label">Rg/Ie:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['rgie']; ?></p>
                            </div>
                        </div>

            <div class="form-group">
                            <label for="finTipo" class="col-sm-2 control-label">Tipo:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"
                                <?php if ($venda['inTipo'] == PESSOA_FISICA) { ?>
                                       <span class="label label-success">Fisica</span>
                                       <?php } else { ?>
                                        <span class="label label-warning">Juridica</span>
                                    <?php } ?></p>
                            </div>
                            <label for="ftelefone" class="col-sm-2 control-label">Telefone:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['telefone']; ?></p>
                            </div>
                            <label for="fcelular" class="col-sm-2 control-label">Celular:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['celular']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fcelular" class="col-sm-2 control-label">CEP:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['cep']; ?></p>
                            </div>
                            <label for="fendereco" class="col-sm-2 control-label">Endereço:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['endereco']; ?></p>
                            </div>
                            <label for="fbairro" class="col-sm-2 control-label">Bairro:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['bairro']; ?></p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="fnumero" class="col-sm-2 control-label">Número:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['numero']; ?></p>
                            </div>
                             <label for="femail" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['email']; ?></p>
                            </div>
                             <label for="fvendedor" class="col-sm-2 control-label">Vendedor:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $venda['usuarioNome']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>

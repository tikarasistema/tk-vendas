<?php

date_default_timezone_set('America/Sao_Paulo');

define('DIRETORIO', realpath('.'));

/*
Temas: default, bootstrap

Temas bootswatch: cosmo, cyborg, darkly, journal, readable, sandstone, simplex, slate, superhero, yeti
Mais temas em http://bootswatch.com/
*/
//define('TWITTER_BOOTSTRAP_TEMA', 'cosmo');

define('TWITTER_BOOTSTRAP_TEMA', 'cosmo');

define('BD_HOST', 'localhost');
define('BD_USUARIO', 'root');
define('BD_SENHA', '');
define('BD_NOME', 'wagashi');

define('CATEGORIA_INATIVO', '0');
define('CATEGORIA_ATIVO', '1');

define('CLIENTE_INATIVO', '0');
define('CLIENTE_ATIVO', '1');

define('PRODUTO_INATIVO', '0');
define('PRODUTO_ATIVO', '1');
define('PRODUTO_MANUTENCAO', '2');

define('USUARIO_INATIVO', '0');
define('USUARIO_ATIVO', '1');

define('VENDA_ABERTA', '0');
define('VENDA_FECHADA', '1');

define('ENCOMENDA_ABERTA', '0');
define('ENCOMENDA_FECHADA', '1');

define('COMPRA_ABERTA', '0');
define('COMPRA_FECHADA', '1');

define('PESSOA_FISICA', '1');
define('PESSOA_JURIDICA', '2');

define('PAGAR_PENDENTE', '0');
define('PAGAR_PAGO', '1');

define('CONDPGTO_INATIVO', '0');
define('CONDPGTO_ATIVO', '1');

define('SEM_ENTRADA', 0);
define('COM_ENTRADA', 1);

define('RECEBER', 1);
define('PAGAR', 2);

define('PARCELA_ABERTA', 'A');
define('PARCELA_BAIXADA', 'B');
define('PARCELA_ESTORNADA', 'E');
define('PARCELA_BAIXADA_PARCIAL_ESTORNO', 'EP');
define('PARCELA_BAIXA_PARCIAL', 'BP');


define('AMORTIZACAO_BAIXA', 1);
define('AMORTIZACAO_ESTORNO', 2);
define('AMORTIZACAO_BAIXA_DINHEIRO', 3);
define('AMORTIZACAO_BAIXA_CARTAO', 4);
<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

if (isset($_POST['idcondpgto'])) {
    $idcondicaopgto = (int) $_POST['idcondpgto'];
} else {
    $idcondicaopgto = (int) $_GET['idcondpgto'];
}

$sql = "Select idcondpgto,ds_condicao, qtde_parcela,situacao, entrada from condpgto where idcondpgto = $idcondicaopgto";
$resultado = mysqli_query($con, $sql);
$registro = mysqli_fetch_assoc($resultado);
if (!$registro) {
    javascriptAlertFim('Condição de pagamento inexistente.', 'condicao-pagamento.php');
}


if ($_POST) {
    $ds_condicao = $_POST['ds_condicao'];
    $entrada = $_POST['entrada'];
    if ($entrada) {
        $entrada = COM_ENTRADA;
    } else {
        $entrada = SEM_ENTRADA;
    }
    $qtde_parcela = $_POST['qtde_parcela'];
    $idcondicaopgto = $_POST['idcondpgto'];
   
    if (isset($_POST['situacao'])) {
        $situacao = CONDPGTO_ATIVO;
    } else {
        $situacao = CONDPGTO_INATIVO;
    }
    
    if (empty($ds_condicao)) {
        $msg[] = 'Campo descrição esta vazio, insira uma descrição';
    }
    if (($entrada != COM_ENTRADA) and ( $entrada != SEM_ENTRADA)) {
        $msg[] = 'Campo entrada esta com valor inválido, selecione um valor válido';
    }
    if (empty($qtde_parcela) and ( $qtde_parcela != 0)) {
        $msg[] = 'Campo quantidade de parcelas esta vazio, digite quantidade';
    }
    if ($qtde_parcela < 0) {
        $msg[] = 'Campo quantidade de parcelas esta com valor inválido, digite valor válido';
    }


    if (!$msg) {
        $sql = "Update condpgto Set
        ds_condicao = '$ds_condicao',
        qtde_parcela = '$qtde_parcela',
        entrada = '$entrada',
        situacao = '$situacao'
        Where idcondpgto = $idcondicaopgto";
     
        $update = mysqli_query($con, $sql);
        if (!$update) {
            $msg[] = 'Falha para salvar a condição de pagamento';
            $msg[] = mysqli_error($con);
            $msg[] = $sql;
        } else {
            javascriptAlertFim('Condição de pagamento foi atualizado', 'condicao-pagamento.php');
        }
    }
} else {
    $ds_condicao = $registro['ds_condicao'];
    $qtde_parcela = $registro['qtde_parcela'];
    $entrada = $registro['entrada'];
    $situacao = $registro['situacao'];
    $idcondicaopgto = $registro['idcondpgto'];
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Editar condição de pagamento</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-building"></i> Editar condição de pagamento: #<?php echo $idcondicaopgto; ?></h1>
                    </div>
                </div>
            </div>

            <?php
            if ($msg) {
                msgHtml($msg);
            }
            ?>

            <form class="row" role="form" method="post" action="condicao-pagamento-editar.php">
                <input type="hidden" name="idcondpgto" value="<?php echo $idcondicaopgto; ?>">
              <div class="col-xs-12">

          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label for="fds_condicao">Descrição</label>
                <input type="text" class="form-control" id="fds_condicao" name="ds_condicao" placeholder="Descrição"
                value="<?php echo $ds_condicao; ?>">
              </div>
            </div>
          </div>
         <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label for="fds_qtde_parcela">Número de parcelas</label>
                <input type="number" class="form-control" id="fqtde_parcela" name="qtde_parcela" placeholder="Número de parcela"
                value="<?php echo $qtde_parcela; ?>">
              </div>
            </div>
          </div>
            <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label for="fsituacao">
                                    <input type="checkbox" name="situacao" id="fsituacao"
                                           <?php if ($situacao == CONDPGTO_ATIVO) { ?> checked<?php } ?>
                                           > Condição pagamento ativo/inativo
                                </label>
                            </div>
                            <div class="help-block" >Desmarque o checkbox caso queira inativar a condição de pagamento.</div>
                        </div>   

                    </div>
                  
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="checkbox">
                              <label for="fativo">
                                  <input type="checkbox" name="entrada" id="fativo"
                                         <?php if ($entrada == COM_ENTRADA) { ?> checked<?php } ?>
                                         > com/sem Entrada
                              </label>
                          </div>
                          <div class="help-block" >Desmarque o checkobox caso não queira dar entrada na condição de pagamento .</div>
                      </div>  
                  </div>

          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary">Salvar</button>
              <button type="reset" class="btn btn-danger">Cancelar</button>
            </div>
          </div>
        </div>
            </form>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
$periodo = (int) $_GET['periodo'];

$vendaTotal = 0;
$vendavlTotal = 0;
$valorPagoTotal = 0;

        $sql = 'Select
        v.idvenda,
        c.idcliente,
        v.data vendaData,
        c.nome clienteNome,
        vi.preco valorVenda,
        vi.precopago valorPago,
        vi.qtd
        From venda v
        Inner Join cliente c
        On c.idCliente = v.idCliente
        Inner Join vendaitem vi
        On vi.idvenda = v.idvenda';
// Where
        $where = array();
        $where[] = "(v.situacao = '" . VENDA_FECHADA . "')";

        switch ($periodo) {
            case 1:
                $periodo = strtotime('today - 7day');
                break;
            case 2:
                $periodo = strtotime('today - 15day');
                break;
            case 3:
                $periodo = strtotime('today - 30day');
                break;
            case 4:
                $periodo = strtotime('today - 3month');
                break;
            case 5:
                $periodo = strtotime('first day of January');
                break;
            case 6:
                $periodo = strtotime('today - 1year');
                break;
        }
            $where[] = "(v.data >='" . date('Y-m-d', $periodo) . "')";
            $sql .= "\nWhere " . join(' and ', $where);
          
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Compras - Valor de venda x Valor pago</title>

    <?php headCss(); ?>
  </head>
  <body>

<?php include 'nav.php'; ?>

<div class="container">

<div class="page-header">
  <h1><i class="fa fa-reorder"></i> Compras - Valor de venda x Valor pago</h1>
</div>

<div class="panel panel-default">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Cliente</th>
        <th>Data</th>
        <th>Valor de venda</th>
        <th>Valor pago</th>
        <th>Diferença</th>
      </tr>
    </thead>
    <tbody>
        <?php
        $consulta = mysqli_query($con,$sql);
        while($resultado = mysqli_fetch_assoc($consulta)){
          $vendaData = date('d-m-Y', strtotime($resultado['vendaData']));
          $valorVenda = $resultado['valorVenda'];
          $valorPago  = $resultado['valorPago'];
          $vendaDifValores = $valorVenda - $valorPago;
          
           $total = $resultado['qtd'] * ($resultado['valorVenda']- $resultado['valorPago']);
           $vendaTotal += $total;  
           
           $totalvlVenda = $resultado['qtd'] * ($resultado['valorVenda']);
           $vendavlTotal += $totalvlVenda; 
           
           $totalvlPago = $resultado['qtd'] * ($resultado['valorPago']);
           $valorPagoTotal += $totalvlPago; 
      ?>
      <tr>
        <td><?php echo $resultado['idvenda']; ?></td>
        <td><?php echo $resultado['clienteNome']; ?></td>
        <td><?php echo $vendaData; ?></td>
        <td>R$ <?php echo number_format($resultado['valorVenda'], 2, ",", "."); ?></td>
        <td>R$ <?php echo number_format($resultado['valorPago'], 2, ",", "."); ?></td>
        <td>R$ <?php echo number_format($vendaDifValores, 2, ",", "."); ?></td>
      </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
    <div class="panel panel-default">
        <div class="panel-body">
            <p> Total no valor de venda R$: <?php echo number_format($vendavlTotal, 2, ",", "."); ?></p>
            <p> Total no valor pago R$: <?php echo number_format($valorPagoTotal, 2, ",", "."); ?></p>
            <p> Diferença no total entre o valor de venda x valor pago R$: <?php echo number_format($vendaTotal, 2, ",", "."); ?></p> 
        </div>
</div>
</div>

<script src="./lib/jquery.js"></script>
<script src="./lib/bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
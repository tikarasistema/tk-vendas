<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$situacao = (int) $_GET['situacao'];
$data_atual = date('d/m/Y');

?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Estoque de produtos</title>

    <?php headCss(); ?>
  </head>
  <body>

<?php include 'nav.php'; ?>

<div class="container">

<div class="page-header">
  <h1><i class="fa fa-reorder"></i> Estoque de produtos</h1>
</div>
<div class="panel panel-default">
        <div class="panel-body">
            <p>Nome:<strong> Tikara sistemas</strong></p>
            <p>Endereço: Praça raposo tavares, no. 210, zona 01</p>
            <p>Complemento: Centro</p>
            <p>CEP: 87200-171</p>
            <p>Celular: (44) 99821-1833</p>
            <p>E-mail: mamiya.ads@gmail.com</p>
            <p>Data de emissão: <strong> <?php echo $data_atual; ?></strong></p>       
        </div>
</div>
<div class="panel panel-default">
    
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Status</th>
        <th>Descrição</th>
        <th>Saldo(Ps)</th>
      </tr>
    </thead>
    <tbody>
        <?php
             $sql = "select sum(saldo) as total from produto where situacao in ($situacao) ";    
                $rtotal= mysqli_query($con, $sql);
                $total = mysqli_fetch_assoc($rtotal);
              
                
            $sql = "select idproduto, produto, saldo from produto where situacao in ($situacao) order by produto asc";    
                $consulta = mysqli_query($con, $sql);
                 while($linha = mysqli_fetch_assoc($consulta)) {
                ?>
                <tr>
                  <td><?php echo $linha['idproduto']; ?></td>
                  <td>
                    <?php if ($situacao == PRODUTO_ATIVO) { ?>
                    <span class="label label-success">ativo</span>
                    <?php } else { ?>
                    <span class="label label-warning">inativo</span>
                    <?php } ?>
                  </td>
                  <td><?php echo $linha['produto']; ?></td>
                   <td><?php echo $linha['saldo']; ?></td>
                </tr>
                <?php } ?>
      <tr>
    </tbody>
  </table>
</div>
<div class="panel panel-default footer">
        <div class="panel-body footer">
            <p> Total de peças: <strong> <?php echo $total['total']; ?>  ps</strong></p>
        </div>
</div>
</div>

<script src="./lib/jquery.js"></script>
<script src="./lib/bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
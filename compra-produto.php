<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
$msgOk = array();
$msgAviso = array();


if (!isset($_SESSION['idcompra'])) {
    header('location:compras.php');
    exit;
}

$idcompra = $_SESSION['idcompra'];
$sql = "Select
	e.idcompra,
	e.data,
	c.nome clienteNome,
        c.cpfcnpj,
        c.rgie,
        c.celular,
        c.inTipo,
        c.telefone,
        c.endereco,
        c.cep,
        c.bairro,
        c.numero,
        c.email,
	u.nome usuarioNome
        From compra e
        Inner Join cliente c
	On (c.idcliente = e.idcliente)
        Inner Join usuario u
	On (u.idusuario = e.idusuario)
        Where
        (e.idcompra = $idcompra)
        And (e.situacao = " . COMPRA_ABERTA . ")";
        $consulta = mysqli_query($con, $sql);
        $compra = mysqli_fetch_assoc($consulta);
        if (!$compra) {
         header('location:compras.php');
         exit;
        }
/*
  Valores para acao
  1 = Incluir produto na venda
  2 = Remover produto na venda
  3 = Alterar 
 * 
 */
$acao = 0;
if (isset($_GET['acao'])) {
    $acao = (int) $_GET['acao'];
} elseif (isset($_POST['acao'])) {
    $acao = (int) $_POST['acao'];
}
if ($acao == 1) {
    $idproduto = (int) $_POST['idproduto'];

    $sql = "Select * From produto Where (idproduto = $idproduto)";
    $consulta = mysqli_query($con, $sql);
    $produto = mysqli_fetch_assoc($consulta);
    $precoProduto = $produto['precovenda'];
    $saldo = $produto['saldo'];
    $precoPago = $_POST['precopago'];
    $qtd = $_POST['qtd'];
    
        //$qtd vem do post digitado pelo usuario e compara com o saldo da tabela de produto
    if ($qtd > $saldo) {
        $msgAviso[] = " Você tentou adicionar uma quantidade superior comparado ao saldo disponível ";
    } else if ($precoPago < 0) {
        $msgAviso[] = 'Você tentou adicionar um valor negativo ';
    } else {
        if ($saldo >= 0) {
            //$sql = "INSERT INTO vendaitem (idproduto, idvenda, preco, precopago, qtd)VALUES($idproduto, $idvenda, $precoProduto, $precoPago, $qtd)";
            $sql = "INSERT INTO compraitem (idproduto, idcompra, preco, precopago, qtd)VALUES($idproduto, $idcompra, $precoProduto, $precoPago, $qtd)";
            $inserir = mysqli_query($con, $sql);
            //adiciona os produtos desejados no item da venda
            if ($inserir) {
                $msgOk[] = "Adicionado $qtd x " . $produto['produto'];
            } else {
                $msgAviso[] = "Erro para inserir o produto na compra: " . mysqli_error($con);
            }
        } else {
            $msgAviso[] = 'Não foi possível adicionar este produto pois está sem saldo';
        }

        // baixa saldo do produto
        if ($saldo <= 0) {
            $msgAviso[] = "Saldo insuficiente, verificar com o administrador ";
        } else {
            $saldoatual = $saldo - $qtd;
            $sql = "UPDATE produto SET saldo = '$saldoatual' WHERE idproduto = $idproduto";
            $resultado = mysqli_query($con, $sql);

            if (!$resultado) {
                $msg[] = 'Falha ao salvar o saldo!';
                $msg[] = mysqli_error($con);
            } else {
                $msgOk[] = "Saldo atualizado com sucesso !";
            }
        }
    }
}
if ($acao == 2) {
    $idproduto = (int) $_GET['idproduto'];
    
        //select da dos itens da venda
    $sql = "Select * From compraitem Where (idproduto = $idproduto)";
    $consulta = mysqli_query($con, $sql);
    $produto = mysqli_fetch_assoc($consulta);
    $qtd = $produto['qtd'];

    //select dos produtos
    $sql = "Select * From produto Where (idproduto = $idproduto)";
    $consul = mysqli_query($con, $sql);
    $prod = mysqli_fetch_assoc($consul);
    $saldo = $prod['saldo'];

    //aqui faz a magica para estornar o saldo
    $saldoestornar = $saldo + $qtd;
    
    $sql = "UPDATE produto SET saldo = '$saldoestornar' WHERE idproduto = $idproduto";
    $resultado = mysqli_query($con, $sql);

    if (!$resultado) {
        $msg[] = 'Falha ao estornar o saldo!';
        $msg[] = mysqli_error($con);
    } else {
        $msgOk[] = "Saldo foi estornado com sucesso ";
    }

    $sql = "Delete From compraitem Where (idproduto = $idproduto)";
    $consulta = mysqli_query($con, $sql);

    $msgOk[] = "Produto removido da encomenda";
}


if ($acao == 3) {

$sql = "Select idusuario, nome, nivelusu From usuario
  Where idusuario = idusuario
    And (situacao = '" . USUARIO_ATIVO . "')";
$consulta = mysqli_query($con, $sql);
$usuario = mysqli_fetch_assoc($consulta);

if ($usuario) {
    $_SESSION['libera'] = 1;
    $_SESSION['idusuario'] = $usuario['idusuario'];
    $_SESSION['nome'] = $usuario['nome'];
    $usuario = $_SESSION['nome'];
}

  $sql = "SELECT idcompra, count(*)as contador FROM `contaspagar` WHERE idcompra = $idcompra group by idcompra";
  $r = mysqli_query($con, $sql);
  $m = mysqli_fetch_assoc($r);

  if($m['contador'] >= 1){
    javascriptAlertFim('Atenção! \n Este registro de pagamento já foi efetuada. ','compra-produto.php');
  }
  



    $total = '';
    $desconto = '';
    $pagamento = '';
    $vl_pagamento = '';
    $idpagar =0;
   
    
if ($_POST) {
    $total = $_POST['total'];
    $desconto = $_POST['desconto'];
    $num_parcela = $_POST['num_parcela'];
    $vl_pagamento = $_POST['vl_pagamento'];
    $entrada = $_POST['entrada'];
    $data_operacao = date('Y-m-d');
    
    
    if (is_numeric($total) && is_numeric($desconto) && is_numeric($vl_pagamento)) {
       $troco = $total - $desconto - $vl_pagamento;
       $troco = number_format($troco, 2,',','.');
    }else{ 
        $msgAviso[] = 'Atenção, valor não numérico encontrado';
    } 
    
    if (is_numeric($total) && is_numeric($desconto)) {
       $tl = $total - $desconto;
       $total_liquido = number_format($tl, 2,',','.');
    }else{ 
        $msgAviso[] = 'Atenção, valor não numérico encontrado';
    }
    
    
   
    if ($entrada =='') {
        $msgAviso[] = 'Informe a entrada';
    }
    if ($total =='') {
        $msgAviso[] = 'Informe o total';
    }
    if ($desconto == '') {
        $msgAviso[] = 'Informe o desconto';
    }
    if ($vl_pagamento == '') {
        $msgAviso[] = 'Informe o pagamento';
    } 
     
    if (!$msgAviso) {
  
       $sql = "INSERT INTO contaspagar(data_operacao, idpagar,idcompra,total,vl_pagamento,troco,desconto,num_parcela,entrada,total_liquido, usuario) "
         . "VALUES ('$data_operacao','$idpagar','$idcompra','$total','$vl_pagamento','$troco','$desconto','$num_parcela','$entrada','$total_liquido','$usuario')";       
        $r = mysqli_query($con, $sql);
        if (!$r) {
            $msgAviso[] = 'Erro para salvar o pagamento';
            $msgAviso[] = mysqli_error($con);
        } else {
            javascriptAlertFim('Pagamento realizado com sucesso \n Troco: R$ '.$troco.'', 'compra-produto.php');
            
        }
    }        
 }
}

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Produtos da compra</title>

<?php headCss(); ?>
    </head>
    <body>

<?php include 'nav.php'; ?>

        <div class="container">

            <div class="page-header">
                <h1><i class="fa fa-shopping-cart"></i> Andamento da compra #<?php echo $idcompra; ?></h1>
            </div>

<?php if ($msgOk) {
    msgHtml($msgOk, 'success');
} ?>
            <?php if ($msgAviso) {
                msgHtml($msgAviso, 'warning');
            } ?>

            <form role="form" method="post" action="compra-produto.php">
                <input type="hidden" name="acao" value="1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Adicionar produto</h3>
                    </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-8">
                                    <div class="form-group">
                                        <label for="fidproduto">Produto</label>
                                        <select id="fidproduto" name="idproduto" class="form-control" required>
                                            <option value="">Selecione um produto</option>
                                            <?php
                                            $sql = 'Select * From produto Where situacao=' . PRODUTO_ATIVO;
                                            $result = mysqli_query($con, $sql);
                                            while ($linha = mysqli_fetch_assoc($result)) {
                                                ?>
                                                <option value="<?php echo $linha['idproduto']; ?>"><?php echo $linha['produto']; ?> (R$ <?php echo number_format($linha['precovenda'], 2, ",", "."); ?>) ------------ Saldo: <?php echo $linha['saldo'].' peças' ;?></option>
<?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="fqtd">Quantidade</label>
                                        <input type="number" class="form-control" id="fqtd" value="0" name="qtd" min="1" required>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="fpreco">Preço unitário</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="fprecopago" name="precopago" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Inserir</button>
                        <button type="reset" class="btn btn-danger">Limpar</button>
                    </div>
                </div>
            </form>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Item (s) adicionados na compra</h3>
                </div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Qtd.</th>
                            <th class="text-center">Produto</th>
                            <th class="text-center">Preço unitário</th>
                            <th class="text-center">Preço total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "Select ei.idproduto,p.produto,ei.precopago,ei.qtd From compraitem ei
                                Inner Join produto p On (p.idproduto = ei.idproduto)
                                Where (ei.idcompra = $idcompra)";
                        
                        $consulta = mysqli_query($con, $sql);
                        $compraTotal = 0;

                        while ($produto = mysqli_fetch_assoc($consulta)) {
                            $total = $produto['qtd'] * $produto['precopago'];
                            $compraTotal += $total;
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $produto['qtd']; ?></td>
                                <td class="text-center"><?php echo $produto['produto']; ?></td>
                                <td class="text-center">R$ <?php echo number_format($produto['precopago'], 2, ',', '.'); ?></td>
                                <td class="text-center">R$ <?php echo number_format($total, 2, ',', '.'); ?></td>
                                <td class="text-center"><a href="compra-produto.php?acao=2&idproduto=<?php echo $produto['idproduto']; ?>" title="Remover produto da compra"><i class="fa fa-times fa-lg"></i></a></td>
                            </tr>
<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th colspan="2">Total da compra</th>
                            <th>R$ <?php echo number_format($compraTotal, 2, ',', '.'); ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
                        
            <form role="form" method="post" action="compra-produto.php">
                <input type="hidden" name="acao" value="3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Forma de pagamento</h3>
                    </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row">                               
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="ftotal">Sub Total</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                           <input type="number" class="form-control" id="ftotal" name="total" placeholder="Ex: 400.85" value="<?php echo $total; ?>">
                                        </div>
                                    </div>
                                </div>     
                                                           
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="fdesconto">Desconto</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control" id="fdesconto" name="desconto" placeholder="Desconto ">
                                    </div> 
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="fpagamento">Pagamento em dinheiro</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control" id="fvl_pagamento" name="vl_pagamento" placeholder="Ex: 100.00">
                                    </div>
                                    </div>
                                </div>
                                
                                <div class="col-xs-12 col-sm-3 col-md-3">                  
                                    <div class="form-group">
                                       <label for="fnum_parcela">Número de parcela</label>
                                       <div class="input-group">
                                            <span class="input-group-addon"></span>
                                       <input type="number" pattern="[0-9]+$" min="0" class="form-control" id="fnum_parcela" placeholder="No. de parcela" name="num_parcela">
                                    </div>
                                    </div>
                                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">                  
                                    <div class="form-group">
                                       <label for="fentrada">Entrada</label>
                                       <input type="number"  pattern="[0-9]+$" min="0" max="1" class="form-control" id="fentrada" placeholder="0-sem entrada/ 1 com entrada" name="entrada">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Efetuar pagamento</button>
                        <button type="reset" class="btn btn-danger">Limpar</button>
                    </div>
                </div>
            </form> 
                       <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Pagamento efetuado</h3>
                </div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Compra</th>
                            <th class="text-center">Sub Total R$</th>
                            <th class="text-center">Desconto R$</th>
                            <th class="text-center">Dinheiro R$</th>
                            <th class="text-center">Troco R$</th>
                            <th class="text-center">Liquido R$</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select idpagar,idcompra,total,vl_pagamento,desconto,troco from contaspagar where idcompra = $idcompra";                        
                        $consulta = mysqli_query($con, $sql);
                        $liquido =0;
                        while ($res = mysqli_fetch_assoc($consulta)) {
                           $liquido = $res['total']-$res['desconto'];
                           
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $res['idpagar']; ?></td>
                                <td class="text-center"><?php echo $res['idcompra']; ?></td>
                                <td class="text-center">R$ <?php echo number_format($res['total'], 2, ',', '.'); ?></td>
                                <td class="text-center">R$ <?php echo number_format($res['desconto'], 2, ',', '.'); ?></td>
                                <td class="text-center">R$ <?php echo number_format($res['vl_pagamento'], 2, ',', '.'); ?></td>
                                <td class="text-center">R$ <?php echo number_format($res['troco'], 2, ',', '.'); ?></td>
                                <td class="text-center">R$ <?php echo number_format($liquido, 2, ',', '.'); ?></td>
                                <td></td>
                            </tr>
<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="6">Total Liquido R$</th>
                            <th>R$ <?php echo number_format($liquido, 2, ',', '.'); ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
                       
            <form class="form-horizontal" method="post" action="compra-fechar.php">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fechamento da compra</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="fidcompra" class="col-sm-2 control-label">Código:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $idcompra; ?></p>
                            </div>

                            <label for="fdata" class="col-sm-2 control-label">Data:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo date('d/m/Y', strtotime($compra['data'])); ?></p>
                            </div>

                            <label for="ftotal" class="col-sm-2 control-label">Total:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static">R$ <?php echo number_format($compraTotal, 2, ',', '.'); ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fcliente" class="col-sm-2 control-label">Cliente:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['clienteNome']; ?></p>
                            </div>
                            <label for="fcpf" class="col-sm-2 control-label">Cpf/Cnpj:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['cpfcnpj']; ?></p>
                            </div>
                             <label for="frgie" class="col-sm-2 control-label">Rg/Ie:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['rgie']; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="finTipo" class="col-sm-2 control-label">Tipo:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"
                                <?php if ($compra['inTipo'] == PESSOA_FISICA) { ?>
                                       <span class="label label-success">Fisica</span>
                                       <?php } else { ?>
                                        <span class="label label-warning">Juridica</span>
                                    <?php } ?></p>
                            </div>
                            <label for="ftelefone" class="col-sm-2 control-label">Telefone:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['telefone']; ?></p>
                            </div>
                            <label for="fcelular" class="col-sm-2 control-label">Celular:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['celular']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fcelular" class="col-sm-2 control-label">CEP:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['cep']; ?></p>
                            </div>
                            <label for="fendereco" class="col-sm-2 control-label">Endereço:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['endereco']; ?></p>
                            </div>
                            <label for="fbairro" class="col-sm-2 control-label">Bairro:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['bairro']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fnumero" class="col-sm-2 control-label">Número:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['numero']; ?></p>
                            </div>
                             <label for="femail" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['email']; ?></p>
                            </div>
                             <label for="fvendedor" class="col-sm-2 control-label">Vendedor:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['usuarioNome']; ?></p>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Fechar compra</button>
                        <p class="form-control-static pull-right"><strong>TOTAL:</strong> R$ <?php echo number_format($compraTotal, 2, ',', '.'); ?></p>
                    </div>
                </div>
            </form>

        </div>

        <script src="./js/compra-produto.js"></script>
        <script src="./lib/jquery.js"></script>
        <script src="./lib/mask.min.js"></script>
        <script src="./lib/jquery-3.2.1.min.js"></script>
        <script src="./lib/jquery.maskMoney.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>

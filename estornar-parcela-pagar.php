<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

if (isset($_POST['idparcela'])) {
    $idparcela = (int) $_POST['idparcela'];
} else {
    $idparcela = (isset($_GET['idparcela']) ? $_GET['idparcela'] : null);
}

if (isset($_POST['idcompra'])) {
    $idcompra = (int) $_POST['idcompra'];
} else {
    $idcompra = (isset($_GET['idcompra']) ? $_GET['idcompra'] : null);
}

$sql = "Select idparcela, data_movimento, vencimento_movimento, pagamento_movimento, valor_movimento, numero_parcela, situacao_parcela,data_pagamento_parcela, total_pago From contaspagarparcelas Where idparcela = $idparcela";
$resultado = mysqli_query($con, $sql);
$registro = mysqli_fetch_assoc($resultado);
$vencimento_movimento = $registro['vencimento_movimento'];
$total_pagoparcela = $registro['total_pago'];
$datahj = date('Y-m-d');
if (!$registro) {
    javascriptAlertFim('Parcela não existe.', 'gerar-contas-parcela.php');
} 

// traz a soma do valor da parcela da tabela amortizacao
$sql = "SELECT sum(vlr_pago) as vlr_pago FROM amortizacao_pagar where idparcela = $idparcela";
$res2 = mysqli_query($con, $sql);
$result = mysqli_fetch_assoc($res2);
if (!$result) {
    $msg[] = 'Falha ao atualizar a parcela ';
    $msg[] = mysqli_error($con);
    $msg[] = $sql;
} else {
    $vlrPago = $result['vlr_pago'];
    $vlrPago < 0 ? 0 : $vlrPago;
}


if ($_POST) {

    $valorEstorno = $_POST['valorEstorno'];
  
    if ($total_pagoparcela > $vlrPago) {
        $msg[] = 'Erro no pagamento: Valor de estorno deve ser menor ou igual valor pago da parcela';
    }
    if ($valorEstorno < 0) {
        $msg[] = 'Erro no pagamento: Estorno deve ser maior que zero';
    }
    if (!$valorEstorno) {
        $msg[] = 'Erro no pagamento: Estorno deve ser maior que zero';
    }

    if (!$msg) {
            $sql = "Select idmovimento, vencimento_movimento,pagamento_movimento, valor_movimento, data_pagamento_parcela From contaspagarparcelas Where idparcela = $idparcela";
            $res = mysqli_query($con, $sql);
            $res = mysqli_fetch_assoc($res);
            $dt_pagto = date('Y-m-d H:i:s');
            $vlr_pago = $valorEstorno * -1;
            $vlr_movimento = $res['valor_movimento'];
            $idcompra = $res['idmovimento'];
            $idusuario = $_SESSION['idusuario'];
            $tipo = AMORTIZACAO_ESTORNO;

            $sql = "Insert into amortizacao_pagar (idparcela, dt_pagto, vlr_pago, idusuario, idcompra, tipo) values ('$idparcela', '$dt_pagto', '$vlr_pago', '$idusuario', '$idcompra', '$tipo'"
                    . ")";
            $insert = mysqli_query($con, $sql);
            if (!$insert) {
                $msg[] = 'Falha ao inserir estorno na parcela ';
                $msg[] = mysqli_error($con);
                $msg[] = $sql;
            }
        } else {
            javascriptAlertFim('Erro: não foi possível inserir os dados na amortização', 'gerar-contas-parcela.php');
        }
        
         if ($valorEstorno == $total_pagoparcela) {
             $data_pagamento_parcela= date('Y-m-d H:i:s'); 
             $situacao_parcela = PARCELA_ABERTA;
             $vlrPago = null;
             $pagamento_movimento = null;
            $sql = "UPDATE contaspagarparcelas SET "
                    . "pagamento_movimento='$pagamento_movimento',data_pagamento_parcela='$data_pagamento_parcela',situacao_parcela='$situacao_parcela',data_pagamento_parcela='$data_pagamento_parcela', total_pago='$vlrPago' WHERE idparcela = $idparcela";

            $update = mysqli_query($con, $sql);
            if (!$update) {
                $msg[] = 'Falha ao alterar a parcela ';
                $msg[] = mysqli_error($con);
                $msg[] = $sql;
            } else {
                javascriptAlertFim('Parcela foi estornada com sucesso', 'gerar-contas-parcela.php?idparcela='.$idparcela);
            }  
         }
            
} else {
    $total_pagoparcela = $registro['total_pago'];
    //$situacao_parcela = $registro['situacao_parcela'];
    //$data = $registro['data_pagamento_parcela'];
    //$vencimento_movimento = $registro['vencimento_movimento'];
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Estorno na parcela</title>

<?php headCss(); ?>
    </head>
    <body>

<?php include 'nav.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-repeat"></i> Estorno na parcela #<?php echo $idparcela; ?></h1>
                    </div>
                </div>
            </div>

<?php
if ($msg) {
    msgHtml($msg);
}
?>

            <form class="row" role="form" method="post" action="estornar-parcela-pagar.php">
                <input type="hidden" name="idparcela" value="<?php echo $idparcela; ?>">
                
                    <div class="container-fluid">
                        <div class="panel panel-default">
                             <div class="panel-heading">Informações da parcela # <?php echo $idparcela; ?></div>
                            <div class="panel-body">
                                <p>Parcela #: <strong><?php echo $idparcela ;?></strong></p>
                                <p>Compra #: <strong><?php echo $idcompra ;?></strong></p>
                                <p>Data de vencimento: <strong><?php echo date('d/m/Y', strtotime($vencimento_movimento)); ;?></strong></p>
                                <p style="color:red;">Valor pago R$:  <strong><?php echo number_format($vlrPago, 2,",", "."); ?></strong></p>
                                 <p>Usuário: <strong><?php echo $_SESSION['nome'] ;?></strong></p>     
                            </div>
                        </div> 
                    </div>

                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fparcela">Valor  de estorno</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control" id="valorEstorno" name="valorEstorno" value="<?php echo $result['vlr_pago'] == null ? number_format(0, 2, '.', '') : $result['vlr_pago']; ?>">
                                    </div>
                                </div>
                            </div>
                        
       
                   
                          
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                    </div>
              
            </form>
        </div>

        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">Histórico de estorno da parcela # <?php echo $idparcela; ?></div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Compra</th>
                            <th class="text-center">Parcela</th>
                            <th class="text-center">Data do Pagamento</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Valor Pago</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
<?php
$totalParcelasAmortizacao = 0;
$sql = "Select tipo,idparcela,dt_pagto,vlr_pago,idusuario,idcompra,idamortizacao from amortizacao_pagar where idparcela = '$idparcela' and idcompra= '$idcompra'";
$consulta3 = mysqli_query($con, $sql);


while ($resultado3 = mysqli_fetch_assoc($consulta3)) {
    $totalParcelasAmortizacao = $totalParcelasAmortizacao + $resultado3['vlr_pago'];
?>
                            <tr>
                                <td class="text-center"><?php echo $resultado3['idamortizacao']; ?></td>
                                <td class="text-center"><?php echo $resultado3['idcompra']; ?></td>
                                <td class="text-center"><?php echo $resultado3['idparcela']; ?></td>
                                <td class="text-center"><?php echo date('d/m/Y', strtotime($resultado3['dt_pagto'])); ?></td>
                                <td class="text-center"><?php
                        switch ($resultado3['tipo']) {
                            case AMORTIZACAO_BAIXA_DINHEIRO:
                                echo 'Pagamento em dinheiro';
                                break;
                             case AMORTIZACAO_BAIXA_CARTAO:
                                echo 'Pagamento com Cartão';
                                break;
                            case AMORTIZACAO_ESTORNO:
                                echo 'Parcela Estornada';
                                break;
                        }
    ?></td>
                        <td class="text-center">R$ <?php echo number_format($resultado3['vlr_pago'], 2, ",", "."); ?></td>
                        <td></td>
                            </tr><?php
                            }?>
                    </tbody>
                    
                </table>
            <div class="panel-footer">
                <p class="form-control-static pull-right">TOTAL PAGO R$: <strong><?php echo number_format($totalParcelasAmortizacao, 2, ',', '.'); ?></strong></p>
            </div>
            </div>
        </div>
       
    </div>
    <script src="./js/pagar-parcela.js"></script>
    <script src="./lib/jquery.js"></script>
    <script src="./lib/mask.min.js"></script>
    <script src="./lib/jquery-3.2.1.min.js"></script>
    <script src="./lib/jquery.maskMoney.js"></script>
    <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
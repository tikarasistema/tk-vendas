<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();
$msg = array();

if (isset($_GET['idusuario'])) {
    $idusuario = (int) $_GET['idusuario'];
} else {
    $idusuario = (int) $_POST['idusuario'];
}

$sql = "Select * From usuario Where idusuario  = $idusuario";
$consulta = mysqli_query($con, $sql);
$retorno = mysqli_fetch_assoc($consulta);
if (!$retorno) {
    echo "Usuário inexistente";
    exit;
}
if ($_POST) {
    $nivelusu = trim($_POST['nivelusu']);
   
    if ($nivelusu== '') {
        $msg[] = "Insira um nível de acesso ao usuário";
    }
    if (!$msg) {
        $nivelusu = $nivelusu;
        $sql = "Update usuario set nivelusu = '$nivelusu' where idusuario = $idusuario";
        $gravou = mysqli_query($con, $sql);
        if ($gravou) {
            $msg[] = "Registro do nível de acesso atualizado";
             javascriptAlertFim('Registro foi salvo com sucesso !', 'usuarios.php');
        } else {
            $msg[] = "Falha ao atualizar dados";
            $msg[] = mysqli_error($con);
        }
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Nível de acesso do usuário</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="page-header">
                <h1><i class="fa fa-lock"></i> Definir nível de acesso: # <?php echo $idusuario; ?></h1>
            </div>

            <?php if ($msg) { msgHtml($msg); } ?>

            <form role="form" method="post" action="usuarios-nivel.php">
        <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>">
                <div class="row">
                                <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fuf">Defina o nível de acesso:</label>
                                <select type="text" class="form-control" id="nivelusu" name="nivelusu">
                                    <option value="">Selecione...</option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Funcionário</option>                                                      
                                </select>
                            </div>
                        </div>
                </div>

                <button type="submit" class="btn btn-primary">Salvar</button>
                <button type="reset" class="btn btn-danger">Cancelar</button>
            </form>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
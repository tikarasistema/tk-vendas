<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
// Pegar idvenda
$idcompra= $_GET['idcompra'];
// Validar idvenda
$sql = "Select idcompra From compra
Where
  (idcompra = $idcompra)
  And (situacao = '" . COMPRA_ABERTA . "')
";
$consulta = mysqli_query($con, $sql);
$compra = mysqli_fetch_assoc($consulta);
if (!$compra) {
  // Nao encontrou a venda
  header('location:compras.php');
  exit;
}
// Criar o idvenda na sessao
$_SESSION['idcompra'] = $idcompra;
// Redirecionar usuario para venda-produto.php
header('location:compra-produto.php');
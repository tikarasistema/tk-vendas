<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

if (isset($_POST['idcliente'])) {
    $idcliente = (int) $_POST['idcliente'];
} else {
    $idcliente = (int) $_GET['idcliente'];
}

$sql = "SELECT idcliente, nome, email, situacao, idcidade, cpfcnpj, rgie, telefone, celular, inTipo, cep, endereco, bairro, numero FROM `cliente` WHERE idcliente = $idcliente";
$resultado = mysqli_query($con, $sql);
$registro = mysqli_fetch_assoc($resultado);

if (!$registro) {
    javascriptAlertFim('Cliente inexistente.', 'clientes.php');
}

if ($_POST) {
    $nome = $_POST['cliente'];
    $email = $_POST['email'];
    $cpfcnpj = $_POST['cpfcnpj'];
    $rgie = $_POST['rgie'];
    $inTipo = $_POST['inTipo'];
    $idcidade = $_POST['cidade'];
    $telefone = $_POST['telefone'];
    $celular = $_POST['celular'];
    $cep = $_POST['cep'];
    $endereco = $_POST['endereco'];
    $bairro = $_POST['bairro'];
    $numero = $_POST['numero'];

    if (isset($_POST['ativo'])) {
        $situacao = CLIENTE_ATIVO;
    } else {
        $situacao = CLIENTE_INATIVO;
    }

    if ($idcidade <= 0) {
        $msg[] = 'Selecione uma cidade';
    } else {
        $sql = "SELECT * FROM cidade
    WHERE idcidade = $idcidade";
        $consulta = mysqli_query($con, $sql);
        $cidade2 = mysqli_fetch_assoc($consulta);
        if (!$cidade2) {
            $msg[] = 'Esta cidade não existe!';
        }
    }

    if ($nome == '') {
        $msg[] = 'Informe o nome/razão social';
    }
    if ($email == '') {
        $msg[] = 'Informe o email';
    }
    if ($cpfcnpj == '') {
        $msg[] = 'Informe o CPF/CNPJ';
    }
    if ($rgie == '') {
        $msg[] = 'Informe o RG/IE';
    }
    if ($telefone == '') {
        $msg[] = 'Informe o telefone';
    }
    if ($celular == '') {
        $msg[] = 'Informe o celular';
    }
    if ($cep == '') {
        $msg[] = 'Informe o CEP';
    }
    if ($endereco == '') {
        $msg[] = 'Informe o endereço';
    }
    if ($bairro == '') {
        $msg[] = 'Informe o bairro';
    }
    if ($numero == '') {
        $msg[] = 'Informe o número';
    }
    if ($inTipo == '') {
        $msg[] = 'Informe o tipo de situação';
    }

    if (!$msg) {
        $sql = "Update cliente Set
                nome = '$nome',
                cpfcnpj = '$cpfcnpj',
                rgie = '$rgie',         
                email = '$email',
                idcidade = '$idcidade',
                cep = '$cep',
                telefone = '$telefone',
                celular = '$celular',
                inTipo = '$inTipo',
                endereco = '$endereco',
                bairro = '$bairro',
                numero = '$numero',
                situacao = '$situacao'
                Where idcliente = $idcliente";

        mysqli_query($con, $sql);

        javascriptAlertFim('Registro salvo', 'clientes.php');
    }
} else {
    $nome = $registro['nome'];
    $email = $registro['email'];
    $cpfcnpj = $registro['cpfcnpj'];
    $rgie = $registro['rgie'];
    $idcidade = $registro['idcidade'];
    $situacao = $registro['situacao'];
    $telefone = $registro['telefone'];
    $celular = $registro['celular'];
    $inTipo = $registro['inTipo'];
    $cep = $registro['cep'];
    $endereco = $registro['endereco'];
    $bairro = $registro['bairro'];
    $numero = $registro['numero'];
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Editar cliente</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-user"></i> Editar cliente #<?php echo $idcliente; ?></h1>
                    </div>
                </div>
            </div>

            <?php
            if ($msg) {
                msgHtml($msg);
            }
            ?>

            <form class="row" role="form" method="post" action="clientes-editar.php">
                <div class="col-xs-12">

                    <input type="hidden" name="idcliente" value="<?php echo $idcliente; ?>">

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="finTipo">Tipo*</label>
                                <select class="form-control" name="inTipo" id="finTipo">
                                    <option value="0">Selecione</option>
                                    <option value="1" <?php echo $inTipo == PESSOA_FISICA ? 'selected' : ''; ?>>Físico</option>
                                    <option value="2" <?php echo $inTipo == PESSOA_JURIDICA ? 'selected' : ''; ?>>Jurídico</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcliente">Nome/Razão social</label>
                                <input type="text" class="form-control" id="fcliente" name="cliente" placeholder="Nome completo" value="<?php echo $nome; ?>">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                              <div class="form-group">
                                <label for="fcpf">CPF/CNPJ</label>
                                <input type="text" class="form-control" id="fcpfcnpj" name="cpfcnpj" placeholder="Digite somente números" size="40" maxlength="18" onBlur="validaFormato(this);" onkeypress="return (apenasNumeros(event))" value="<?php echo $cpfcnpj; ?>">
                            <div id="divResultado"></div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcpf">RG/IE</label>
                                <input type="text" class="form-control" id="frgie" name="rgie" placeholder="Somente números" maxlength="15" value="<?php echo $rgie; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="ftelefone">Telefone</label>
                                <input type="text" class="form-control" data-mask="(00) 0000-0000" id="ftelefone" name="telefone" placeholder="(00) 0000-0000" maxlength="" value="<?php echo $telefone; ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcelular">Celular</label>
                                <input type="text" class="form-control" data-mask="(00) 00000-0000" id="fcelular" placeholder="(00) 00000-0000" name="celular" value="<?php echo $celular; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="femail">Email</label>
                                <input type="text" class="form-control" id="femail" name="email" value="<?php echo $email; ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcidade">Cidade</label>
                                <select class="form-control" id="fcidade" name="cidade">
                                    <option value="0">Selecione uma cidade</option>
                                    <?php
                                    $sql = "Select idcidade, cidade, uf From cidade Order By cidade";
                                    $q = mysqli_query($con, $sql);
                                    while ($cidade = mysqli_fetch_assoc($q)) {
                                        ?>        
                                        <option value="<?php echo $cidade['idcidade']; ?>"
                                                <?php if ($idcidade == $cidade['idcidade']) { ?> selected <?php } ?>
                                                ><?php echo $cidade['cidade']; ?>/<?php echo $cidade['uf']; ?></option>
                                            <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fendereco">Endereço</label>
                                <input type="text" class="form-control" id="fendereco" name="endereco" placeholder="Ex: Rua raposo tavares, zona 01" value="<?php echo $endereco; ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fbairro">Bairro</label>
                                <input type="text" class="form-control" id="fbairro" name="bairro" placeholder="Ex: Zona 01" value="<?php echo $bairro; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="fnumero">Número</label>
                                <input type="text" class="form-control" id="fnumero" name="numero" placeholder="Número" value="<?php echo $numero; ?>">
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="fcep">CEP</label>
                                <input type="text" class="form-control" maxlength="8" pattern= "\d{5}-?\d{3}" id="fcep" name="cep" placeholder="Ex: xxxxx-xxx" value="<?php echo $cep; ?>">
                            </div>
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label for="fativo">
                                    <input type="checkbox" name="ativo" id="fativo"
                                           <?php if ($situacao == CLIENTE_ATIVO) { ?> checked<?php } ?>
                                           > Cliente ativo
                                </label>
                            </div>
                        </div>   

                    </div>
                    <div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary">Salvar</button>
                                <button type="reset" class="btn btn-danger">Cancelar</button>
                            </div>
                        </div>
                    </div>
            </form>

        </div>

         <script src="./js/cliente.js"></script>
        <script src="./lib/jquery.js"></script>
        <script src="./lib/mask.min.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
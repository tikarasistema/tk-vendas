<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tikara sistemas</title>
    <a href="index.php"></a>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>
        <div class="container">
        </div>
        
        <div class="container">
        </div>
        
        <div class="container"> 
            <h4 class="text-center">Bem vindo, <?php echo $_SESSION['nome']; ?>!</h4>
        </div>

        <div class="container">
            <div class="row">
                <?php
                $sql = "select count(idcliente) cli from cliente where situacao = 1";
                $produtos = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($produtos)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-user  fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <div class="col-xs-6x text-right">
                                            <p class="announcement-heading"><?php echo $ativo['cli']; ?></p>
                                        <?php } ?>
                                        <p class="announcement-text"> Cliente ativo</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="clientes.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idproduto) prod from produto where situacao = 1";
                $produtos = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($produtos)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-barcode fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['prod']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Produto ativo</p>
                                </div>
                            </div>
                        </div>
                        <a href="produtos.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idvenda) ven from venda where situacao = 0";
                $vendas = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($vendas)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['ven']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Venda aberta</p>
                                </div>
                            </div>
                        </div>
                        <a href="vendas.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idcompra) compra from compra where situacao = 0";
                $compras = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($compras)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-truck fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['compra']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Compra aberta</p>
                                </div>
                            </div>
                        </div>
                        <a href="compras.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idusuario) usu from usuario where situacao = 1";
                $usuarios = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($usuarios)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['usu']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Usuário ativo</p>
                                </div>
                            </div>
                        </div>
                        <a href="usuarios.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idcidade) cid from cidade where idcidade = idcidade";
                $cidades = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($cidades)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-truck fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['cid']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Cidade ativa</p>
                                </div>
                            </div>
                        </div>
                        <a href="cidades.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idcategoria) cat from categoria where situacao = 1";
                $cat = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($cat)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-truck fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['cat']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Categoria ativa</p>
                                </div>
                            </div>
                        </div>
                        <a href="categorias.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->

        </div>

        <div class="container botao">
            <button type="button" class="btn-toggle fa fa-arrow-circle-right btn-primary" data-element="#minhaDiv"></button>
        </div>
        <div class="container" style="display:none;" id="minhaDiv">


            <div class="row">
                <?php
                $sql = "select count(idcliente) cli from cliente where situacao = 0";
                $produtos = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($produtos)) {
                    ?>

                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-user  fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <div class="col-xs-6x text-right">
                                            <p class="announcement-heading"><?php echo $ativo['cli']; ?></p>
                                        <?php } ?>
                                        <p class="announcement-text"> Cliente inativo</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="clientes.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idproduto) prod from produto where situacao = 0";
                $produtos = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($produtos)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-barcode fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['prod']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Produto inativo</p>
                                </div>
                            </div>
                        </div>
                        <a href="produtos.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idvenda) ven from venda where situacao = 1";
                $vendas = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($vendas)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['ven']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Venda fechada</p>
                                </div>
                            </div>
                        </div>
                        <a href="vendas.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idcompra) compra from compra where situacao = 1";
                $compras = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($compras)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-truck fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['compra']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Compra fechada</p>
                                </div>
                            </div>
                        </div>
                        <a href="encomendas.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                $sql = "select count(idusuario) usu from usuario where situacao = 0";
                $usuarios = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($usuarios)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-user fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['usu']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Usuário ativo</p>
                                </div>
                            </div>
                        </div>
                        <a href="usuarios.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <?php
                $sql = "select count(idcategoria) cat from categoria where situacao = 0";
                $cat = mysqli_query($con, $sql);
                while ($ativo = mysqli_fetch_assoc($cat)) {
                    ?>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <i class="fa fa-truck fa-5x"></i>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="announcement-heading"><?php echo $ativo['cat']; ?></p>
                                    <?php } ?>
                                    <p class="announcement-text"> Categoria inativa</p>
                                </div>
                            </div>
                        </div>
                        <a href="categorias.php">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Visualizar
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div>
        <script src="./lib/jquery.js"></script>
        <script src="./js/index.js"></script>
        <script src="./lib/jquery-3.2.1.min.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>

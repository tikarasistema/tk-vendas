<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

    //select que busca o total de item vendidona encomendaitem
//    $sql2 = "select precopago, qtd, sum( precopago * qtd) as totalitemvenda from compraitem where idcompra = $idcompra";
//    $consulta2 = mysqli_query($con, $sql2);
//    $res = mysqli_fetch_assoc($consulta2);
//    $totalitemvenda = $res['totalitemvenda']; 
    
    //aqui busca o total de pagamento no caixa_pagamento
//    $sql = "select e.idcompra, cp.pagamento_id, sum(cp.pagamento_total) as totalpagamento  from caixa_pagamento cp inner join compra e 
//    on cp.idcompra = e.idcompra
//    where e.idcompra = $idcompra";
//    $consulta = mysqli_query($con, $sql);
//    $r = mysqli_fetch_assoc($consulta);
//    $totalpago = $r['totalpagamento'];



    
  $sql = "Select idusuario, nome, nivelusu From usuario
  Where idusuario = idusuario
    And (situacao = '" . USUARIO_ATIVO . "')";
    $consulta = mysqli_query($con, $sql);
    $usuario = mysqli_fetch_assoc($consulta);

    if ($usuario) {
        $_SESSION['logado'] = 1;
        $_SESSION['idusuario'] = $usuario['idusuario'];
        $_SESSION['nome'] = $usuario['nome'];
        $usuario = $_SESSION['nome'];
    }
    
    $total = '';
    $desconto = '';
    $pagamento = '';
    $pagamento_status = PAGAR_PAGO;
    
if ($_POST) {
    $total = $_POST['total'];
    $desconto = $_POST['desconto'];
    $pagamento = $_POST['pagamento'];
    $dtPagamento = $_POST['dtPagamento'];
    $data = $dtPagamento;
    $transacao = rand(1000, 1000000000);
    //$troco = $total - $desconto - $pagamento;
    
    if (is_numeric($total) && is_numeric($desconto) && is_numeric($pagamento)) {
       $troco = $total - $desconto - $pagamento;
       $troco = number_format($troco, 2,',','.');
    }else{ 
        $msgAviso[] = 'Atenção, valor não numérico encontrado';
    } 
    
    if ($total =='') {
        $msgAviso[] = 'Informe o total';
    }
    if ($desconto == '') {
        $msgAviso[] = 'Informe o desconto';
    }
    if ($pagamento == '') {
        $msgAviso[] = 'Informe o pagamento';
    }
    
//    if($totalitemvenda == $totalpago){
//         $msgAviso[] = 'Esta compra pode ser finalizada, pois o valor comprado e pago estão iguais';       
//    }
      
    if (!$msgAviso) {
        $_SESSION['transacao'] = $transacao;
       $sql = "INSERT INTO caixa_pagamento(pagamento_total, pagamento_desconto, pagamento_dinheiro, pagamento_troco, pagamento_usuario, pagamento_transacao, pagamento_data,pagamento_status,idcompra) "
         . "VALUES ('$total','$desconto','$pagamento','$troco','$usuario','$transacao','$data','$pagamento_status','$idcompra')";       
        $r = mysqli_query($con, $sql);
        if (!$r) {
            $msgAviso[] = 'Erro para salvar o pagamento';
            $msgAviso[] = mysqli_error($con);
        } else {
            javascriptAlertFim('Pagamento realizado com sucesso \n Troco: R$ '.$troco.'', 'compra-produto.php');
            
        }
    } 
 
}

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pagamento do caixa</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-money"></i> Pagamento do caixa</h1>
                    </div>
                </div>
            </div>

            <?php
            if ($msg) {
                msgHtml($msg);
            }
            ?>

            <form class="row" role="form" method="post" action="caixa.php">
            <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-8">
                                    <div class="form-group">
                                        <label for="fidproduto">Produto</label>
                                        <select id="fidproduto" name="idproduto" class="form-control" required>
                                            <option value="">Selecione um produto</option>
                                            <?php
                                            $sql = 'Select * From produto Where situacao=' . PRODUTO_ATIVO;
                                            $result = mysqli_query($con, $sql);
                                            while ($linha = mysqli_fetch_assoc($result)) {
                                                ?>
                                                <option value="<?php echo $linha['idproduto']; ?>"><?php echo $linha['produto']; ?> (R$ <?php echo number_format($linha['precovenda'], 2, ",", "."); ?>) ------------ Saldo: <?php echo $linha['saldo'].' peças' ;?></option>
<?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="fqtd">Quantidade</label>
                                        <input type="number" class="form-control" id="fqtd" value="0" name="qtd" min="1" required>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="fpreco">Preço unitário</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="fprecopago" name="precopago" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                       <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                            <button type="reset" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>
            </form>
        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
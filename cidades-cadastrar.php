<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

$cidade = '';
$uf = '';

if ($_POST) {
    $cidade = $_POST['cidade'];
    $uf = $_POST['uf'];

    if (strlen($cidade) < 2) {
        $msg[] = 'Campo inválido, a cidade deve conter mais de 2 caracteres';
    }
    if ($cidade == '') {
        $msg[] = 'Informe a cidade';
    }
    if ($uf == '') {
        $msg[] = 'Informe a UF';
    }

    if (!$msg) {

        $sql = "Insert Into cidade(cidade, uf) Values('$cidade', '$uf')";
        $r = mysqli_query($con, $sql);
        if (!$r) {
            $msg[] = 'Erro para salvar';
            $msg[] = mysqli_error($con);
        } else {
            javascriptAlertFim('Registro foi salvo com sucesso !', 'cidades.php');
        }
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cadastrar cidade</title>

<?php headCss(); ?>
    </head>
    <body>

<?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-building"></i> Cadastrar cidade</h1>
                    </div>
                </div>
            </div>

<?php
if ($msg) {
    msgHtml($msg);
}
?>

            <form class="row" role="form" method="post" action="cidades-cadastrar.php">
                <div class="col-xs-12">

                    <div class="row">
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="fuf">UF</label>
                                <select type="text" class="form-control" id="uf" name="uf">
                                    <option value="">Selecione...</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-10">
                            <div class="form-group">
                                <label for="fcidade">Cidade</label>
                                <input type="text" class="form-control" id="fcidade" name="cidade" placeholder="Nome da cidade">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                            <button type="reset" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();



if ($_POST) {
    
  $sql = "Select idusuario, nome, nivelusu From usuario
  Where idusuario = idusuario
    And (situacao = '" . USUARIO_ATIVO . "')";
    $consulta = mysqli_query($con, $sql);
    $usuario = mysqli_fetch_assoc($consulta);

    if ($usuario) {
        $_SESSION['libera'] = 1;
        $_SESSION['idusuario'] = $usuario['idusuario'];
        $_SESSION['nome'] = $usuario['nome'];
        $usuario = $_SESSION['nome'];
    }
    
   $saldo = $_POST['saldo'];
   $saldo = str_replace('.',',',$saldo);
   $saldo = str_replace(',','.',$saldo);
    
    $data = date('Y-m-d H:i:s');

    if ($saldo == '') {
        $msg[] = 'Informar o valor do saldo inicial';
    }
     if ($saldo <= 0) {
        $msg[] = 'O saldo inicial está com valor inferior a 0';
    }
    

    if (!$msg) { 
        $sql = "INSERT INTO aberturacaixa(abertura_saldo, abertura_nome, abertura_data) VALUES ('$saldo','$usuario','$data')";
        
        $r = mysqli_query($con, $sql);
        if (!$r) {
            $msg[] = 'Erro ao salvar a abertura do caixa';
            $msg[] = mysqli_error($con);
        } else {
            javascriptAlertFim('Abertura de caixa foi realizada com sucesso !', 'saldoinicial.php');
        }
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Saldo Inicial</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-money"></i> Saldo Inicial</h1>
                    </div>
                </div>
            </div>

            <?php
            if ($msg) {
                msgHtml($msg);
            }
            ?>

            <form class="row" role="form" method="post" action="saldoinicial.php">

                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label for="fsaldo">Saldo Inicial R$</label>
                            <div class="input-group">
                                <span class="input-group-addon">R$</span>
                                <input id="input" type="text" class="form-control" name="saldo" placeholder="Ex: 100,00">
                            </div>
                        </div>
                    </div>
      
                </div> 
                       <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                            <button type="reset" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>
            </form>
        </div>
        <script src="./js/saldoinicial.js"></script>
        <script src="./lib/jquery.js"></script>
        <script src="./lib/mask.min.js"></script>
        <script src="./lib/jquery-3.2.1.min.js"></script>
        <script src="./lib/jquery.maskMoney.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
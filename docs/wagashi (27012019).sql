-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27-Jan-2019 às 22:29
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wagashi`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aberturacaixa`
--

CREATE TABLE `aberturacaixa` (
  `abertura_id` int(11) NOT NULL,
  `abertura_saldo` varchar(60) NOT NULL,
  `abertura_nome` varchar(60) NOT NULL,
  `abertura_data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `aberturacaixa`
--

INSERT INTO `aberturacaixa` (`abertura_id`, `abertura_saldo`, `abertura_nome`, `abertura_data`) VALUES
(1, '1000.00', 'Admin', '2018-12-24 20:06:33'),
(2, '500', 'Admin', '2019-01-02 18:44:03'),
(3, '100.00', 'Admin', '2019-01-19 01:02:59'),
(4, '150', 'Admin', '2019-01-19 01:04:07'),
(5, '100.00', 'Admin', '2019-01-19 01:07:33'),
(6, '120.00', 'Admin', '2019-01-19 01:08:30'),
(7, '1.000.95', 'Admin', '2019-01-19 13:52:45');

-- --------------------------------------------------------

--
-- Estrutura da tabela `amortizacao_pagar`
--

CREATE TABLE `amortizacao_pagar` (
  `tipo` char(1) NOT NULL,
  `idamortizacao` int(11) NOT NULL,
  `idparcela` int(11) NOT NULL,
  `dt_pagto` datetime DEFAULT NULL,
  `vlr_pago` decimal(10,2) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idcompra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `amortizacao_pagar`
--

INSERT INTO `amortizacao_pagar` (`tipo`, `idamortizacao`, `idparcela`, `dt_pagto`, `vlr_pago`, `idusuario`, `idcompra`) VALUES
('3', 1, 1, '2019-01-27 18:37:55', '23.33', 1, 2),
('4', 2, 1, '2019-01-27 18:41:57', '10.00', 1, 2),
('3', 3, 2, '2019-01-27 18:43:52', '3.33', 1, 2),
('3', 4, 2, '2019-01-27 18:44:04', '6.00', 1, 2),
('4', 5, 2, '2019-01-27 18:47:02', '24.00', 1, 2),
('2', 6, 2, '2019-01-27 18:47:11', '-33.33', 1, 2),
('3', 7, 3, '2019-01-27 19:15:49', '33.34', 1, 2),
('3', 8, 2, '2019-01-27 19:20:39', '13.33', 1, 2),
('3', 9, 2, '2019-01-27 19:27:39', '20.00', 1, 2),
('2', 10, 2, '2019-01-27 19:28:38', '-33.33', 1, 2),
('3', 11, 2, '2019-01-27 19:28:49', '3.33', 1, 2),
('2', 12, 2, '2019-01-27 19:28:59', '-3.33', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa_pagamento_original`
--

CREATE TABLE `caixa_pagamento_original` (
  `pagamento_id` int(11) NOT NULL,
  `pagamento_total` varchar(60) NOT NULL,
  `pagamento_desconto` varchar(60) NOT NULL,
  `pagamento_dinheiro` varchar(60) NOT NULL,
  `pagamento_troco` varchar(60) NOT NULL,
  `pagamento_usuario` varchar(60) NOT NULL,
  `pagamento_transacao` int(11) NOT NULL,
  `pagamento_data` datetime NOT NULL,
  `idencomenda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `caixa_pagamento_original`
--

INSERT INTO `caixa_pagamento_original` (`pagamento_id`, `pagamento_total`, `pagamento_desconto`, `pagamento_dinheiro`, `pagamento_troco`, `pagamento_usuario`, `pagamento_transacao`, `pagamento_data`, `idencomenda`) VALUES
(1, '22', '0', '100', '-78,00', 'Admin', 681056454, '2018-12-25 15:29:27', 0),
(2, '22', '0', '100', '-78,00', 'Admin', 708906104, '2018-12-25 15:31:32', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `situacao` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`idcategoria`, `categoria`, `situacao`) VALUES
(1, 'Refrigerante', '1'),
(2, 'Sanduiche', '1'),
(3, 'Doces', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE `cidade` (
  `idcidade` int(11) NOT NULL,
  `cidade` varchar(45) NOT NULL,
  `uf` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`idcidade`, `cidade`, `uf`) VALUES
(1, 'Cianorte', 'CE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `situacao` char(1) NOT NULL,
  `idcidade` int(11) NOT NULL,
  `cpfcnpj` varchar(20) DEFAULT NULL,
  `rgie` varchar(20) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `inTipo` char(1) DEFAULT NULL,
  `cep` int(20) DEFAULT NULL,
  `endereco` varchar(90) DEFAULT NULL,
  `bairro` varchar(60) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nome`, `email`, `situacao`, `idcidade`, `cpfcnpj`, `rgie`, `telefone`, `celular`, `inTipo`, `cep`, `endereco`, `bairro`, `numero`) VALUES
(1, 'Marcelo Miyashita', 'mamiya.ads@gmail.com', '1', 1, '144.143.118-70', '123456987', '(44) 3018-2890', '(44) 9982-1183', '1', 87200171, 'Praça Raposo Tavares', 'centro', '5130'),
(2, 'razao social cnpj', 'teste@gmail.com', '1', 1, '57.091.804/0001-60', '198563254', '(44) 3333-3333', '(44) 9999-5555', '2', 87200171, 'avenida um', 'zona 02', '215');

-- --------------------------------------------------------

--
-- Estrutura da tabela `compra`
--

CREATE TABLE `compra` (
  `idusuario` int(11) NOT NULL,
  `idcompra` int(11) NOT NULL,
  `data` datetime DEFAULT NULL,
  `situacao` char(1) DEFAULT NULL,
  `idcliente` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `compra`
--

INSERT INTO `compra` (`idusuario`, `idcompra`, `data`, `situacao`, `idcliente`) VALUES
(1, 1, '2019-01-26 00:00:00', '1', 2),
(1, 2, '2019-01-27 00:00:00', '1', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `compraitem`
--

CREATE TABLE `compraitem` (
  `idproduto` int(11) NOT NULL,
  `idcompra` int(11) NOT NULL,
  `preco` decimal(8,2) NOT NULL,
  `precopago` decimal(8,2) NOT NULL,
  `qtd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `compraitem`
--

INSERT INTO `compraitem` (`idproduto`, `idcompra`, `preco`, `precopago`, `qtd`) VALUES
(5, 2, '100.00', '100.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `condpgto`
--

CREATE TABLE `condpgto` (
  `idcondpgto` int(11) NOT NULL,
  `ds_condicao` varchar(45) NOT NULL,
  `qtde_parcela` int(11) NOT NULL,
  `entrada` char(1) DEFAULT NULL,
  `situacao` char(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `condpgto`
--

INSERT INTO `condpgto` (`idcondpgto`, `ds_condicao`, `qtde_parcela`, `entrada`, `situacao`) VALUES
(1, '1+1', 2, '1', '1'),
(2, '1+4', 5, '1', '1'),
(3, '5X', 5, NULL, '1'),
(4, '3X', 3, NULL, '1'),
(5, 'À vista', 0, '', '1'),
(6, '1+3', 4, '1', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contaspagar`
--

CREATE TABLE `contaspagar` (
  `data_operacao` date NOT NULL,
  `idpagar` int(11) NOT NULL,
  `idcompra` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `vl_pagamento` decimal(10,2) NOT NULL,
  `troco` decimal(10,2) NOT NULL,
  `desconto` decimal(10,2) NOT NULL,
  `entrada` char(1) NOT NULL,
  `num_parcela` int(11) NOT NULL,
  `total_liquido` decimal(10,2) NOT NULL,
  `usuario` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contaspagar`
--

INSERT INTO `contaspagar` (`data_operacao`, `idpagar`, `idcompra`, `total`, `vl_pagamento`, `troco`, `desconto`, `entrada`, `num_parcela`, `total_liquido`, `usuario`) VALUES
('2019-01-26', 1, 1, '100.00', '100.00', '0.00', '0.00', '1', 3, '100.00', 'Admin'),
('2019-01-27', 2, 2, '100.00', '100.00', '-5.00', '5.00', '0', 3, '95.00', 'Admin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contaspagarparcelas`
--

CREATE TABLE `contaspagarparcelas` (
  `idparcela` int(11) NOT NULL,
  `idmovimento` int(11) DEFAULT NULL,
  `data_movimento` date DEFAULT NULL,
  `vencimento_movimento` date DEFAULT NULL,
  `pagamento_movimento` double(10,2) DEFAULT NULL,
  `valor_movimento` decimal(10,2) DEFAULT NULL,
  `numero_parcela` int(11) NOT NULL,
  `situacao_parcela` char(2) NOT NULL,
  `data_pagamento_parcela` date NOT NULL,
  `total_pago` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contaspagarparcelas`
--

INSERT INTO `contaspagarparcelas` (`idparcela`, `idmovimento`, `data_movimento`, `vencimento_movimento`, `pagamento_movimento`, `valor_movimento`, `numero_parcela`, `situacao_parcela`, `data_pagamento_parcela`, `total_pago`) VALUES
(1, 2, '2019-01-27', '2019-02-26', 10.00, '33.33', 1, 'B', '2019-01-27', '33.33'),
(2, 2, '2019-01-27', '2019-03-28', 0.00, '33.33', 2, 'A', '2019-01-27', '0.00'),
(3, 2, '2019-01-27', '2019-04-27', 33.34, '33.34', 3, 'B', '2019-01-27', '33.34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contaspagar_original`
--

CREATE TABLE `contaspagar_original` (
  `idpagar` int(11) NOT NULL,
  `valor` decimal(8,2) NOT NULL,
  `data_paga` datetime NOT NULL,
  `data_operacao` date NOT NULL,
  `num_parcela` int(11) NOT NULL,
  `entrada` char(1) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `idcliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contaspagar_original`
--

INSERT INTO `contaspagar_original` (`idpagar`, `valor`, `data_paga`, `data_operacao`, `num_parcela`, `entrada`, `usuario`, `idcliente`) VALUES
(1, '100.00', '2019-01-05 00:00:00', '2018-12-31', 3, '1', 'Admin', 1),
(2, '1000.00', '2019-01-19 00:00:00', '2018-12-31', 5, '0', 'Admin', 1),
(3, '100.00', '2019-01-10 00:00:00', '2018-12-31', 3, '0', 'Admin', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `idproduto` int(11) NOT NULL,
  `produto` varchar(100) NOT NULL,
  `precocompra` decimal(8,2) NOT NULL,
  `precovenda` decimal(8,2) NOT NULL,
  `situacao` char(1) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`idproduto`, `produto`, `precocompra`, `precovenda`, `situacao`, `idcategoria`, `saldo`) VALUES
(1, 'Coca Cola Lata', '2.00', '2.50', '1', 1, 0),
(2, 'Coca Cola Garrafa', '2.00', '3.00', '1', 1, 126),
(3, 'Lanche natural', '4.00', '5.00', '1', 2, 0),
(4, 'X-Salada', '7.00', '10.00', '1', 2, 0),
(5, 'Mandyu com 12 unidades - tam.M - Feijão azuki', '50.00', '100.00', '1', 3, 91);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` char(32) NOT NULL,
  `situacao` char(1) DEFAULT NULL,
  `nivelusu` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nome`, `email`, `senha`, `situacao`, `nivelusu`) VALUES
(1, 'Admin', 'admin@admin.com', '001001', '1', '1'),
(2, 'Funcionario', 'funcionario@gmail.com', '001001', '1', '2'),
(3, 'Funcionario 2', 'funcionario2@gmail.com', '001001', '1', '1'),
(4, 'Funcionario 3', 'funcionario3@gmail.com', '001001', '1', '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda`
--

CREATE TABLE `venda` (
  `idvenda` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `idcliente` int(11) NOT NULL,
  `situacao` char(1) NOT NULL,
  `idusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendaitem`
--

CREATE TABLE `vendaitem` (
  `idproduto` int(11) NOT NULL,
  `idvenda` int(11) NOT NULL,
  `preco` decimal(8,2) NOT NULL,
  `precopago` decimal(8,2) NOT NULL,
  `qtd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aberturacaixa`
--
ALTER TABLE `aberturacaixa`
  ADD PRIMARY KEY (`abertura_id`);

--
-- Indexes for table `amortizacao_pagar`
--
ALTER TABLE `amortizacao_pagar`
  ADD PRIMARY KEY (`idamortizacao`),
  ADD KEY `fk_amort_ctapaga_idx` (`idparcela`),
  ADD KEY `fk_amort_usuario_idx` (`idusuario`),
  ADD KEY `fk_tbamortizacao_compra1_idx` (`idcompra`);

--
-- Indexes for table `caixa_pagamento_original`
--
ALTER TABLE `caixa_pagamento_original`
  ADD PRIMARY KEY (`pagamento_id`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indexes for table `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`idcidade`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`),
  ADD KEY `fk_cliente_cidade1_idx` (`idcidade`);

--
-- Indexes for table `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`idcompra`),
  ADD KEY `idusuario` (`idusuario`);

--
-- Indexes for table `compraitem`
--
ALTER TABLE `compraitem`
  ADD PRIMARY KEY (`idproduto`,`idcompra`) USING BTREE,
  ADD KEY `fk_produto_has_encomenda_encomenda1_idx` (`idcompra`),
  ADD KEY `fk_produto_has_encomenda_produto1_idx` (`idproduto`);

--
-- Indexes for table `condpgto`
--
ALTER TABLE `condpgto`
  ADD PRIMARY KEY (`idcondpgto`);

--
-- Indexes for table `contaspagar`
--
ALTER TABLE `contaspagar`
  ADD PRIMARY KEY (`idpagar`,`idcompra`) USING BTREE;

--
-- Indexes for table `contaspagarparcelas`
--
ALTER TABLE `contaspagarparcelas`
  ADD PRIMARY KEY (`idparcela`),
  ADD KEY `idmovimento` (`idmovimento`) USING BTREE,
  ADD KEY `idmovimento_2` (`idmovimento`) USING BTREE;

--
-- Indexes for table `contaspagar_original`
--
ALTER TABLE `contaspagar_original`
  ADD PRIMARY KEY (`idpagar`),
  ADD KEY `fk_cliente_cp1_idx` (`idcliente`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`idproduto`),
  ADD KEY `fk_produto_categoria_idx` (`idcategoria`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indexes for table `venda`
--
ALTER TABLE `venda`
  ADD PRIMARY KEY (`idvenda`),
  ADD KEY `fk_venda_cliente1_idx` (`idcliente`),
  ADD KEY `fk_venda_usuario1_idx` (`idusuario`);

--
-- Indexes for table `vendaitem`
--
ALTER TABLE `vendaitem`
  ADD PRIMARY KEY (`idproduto`,`idvenda`),
  ADD KEY `fk_produto_has_venda_venda1_idx` (`idvenda`),
  ADD KEY `fk_produto_has_venda_produto1_idx` (`idproduto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aberturacaixa`
--
ALTER TABLE `aberturacaixa`
  MODIFY `abertura_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `amortizacao_pagar`
--
ALTER TABLE `amortizacao_pagar`
  MODIFY `idamortizacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `caixa_pagamento_original`
--
ALTER TABLE `caixa_pagamento_original`
  MODIFY `pagamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cidade`
--
ALTER TABLE `cidade`
  MODIFY `idcidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `compra`
--
ALTER TABLE `compra`
  MODIFY `idcompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `condpgto`
--
ALTER TABLE `condpgto`
  MODIFY `idcondpgto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contaspagar`
--
ALTER TABLE `contaspagar`
  MODIFY `idpagar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contaspagarparcelas`
--
ALTER TABLE `contaspagarparcelas`
  MODIFY `idparcela` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contaspagar_original`
--
ALTER TABLE `contaspagar_original`
  MODIFY `idpagar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `idproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `venda`
--
ALTER TABLE `venda`
  MODIFY `idvenda` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_cliente_cidade1` FOREIGN KEY (`idcidade`) REFERENCES `cidade` (`idcidade`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `contaspagar_original`
--
ALTER TABLE `contaspagar_original`
  ADD CONSTRAINT `fk_cliente_cp1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `fk_produto_categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `venda`
--
ALTER TABLE `venda`
  ADD CONSTRAINT `fk_venda_cliente1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venda_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

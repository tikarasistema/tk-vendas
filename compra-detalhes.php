<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
$msgOk = array();
$msgAviso = array();

if (!isset($_GET['idcompra'])) {
    header('location:compras.php');
    exit;
}
$idcompra = (int) $_GET['idcompra'];
$sql = "Select
	e.idcompra,
	e.data,
        c.idcliente,
	c.nome clienteNome,
        c.cpfcnpj,
        c.rgie,
        c.telefone,
        c.endereco,
        c.cep,
        c.bairro,
        c.numero,
        c.celular,
        c.inTipo,
        c.email,
	u.nome usuarioNome
        From compra e
        Inner Join cliente c
	On (c.idcliente = e.idcliente)
        Inner Join usuario u
	On (u.idusuario = e.idusuario)
        Where
        (e.idcompra = $idcompra)
        And (e.situacao = " . COMPRA_FECHADA . ")";
$consulta = mysqli_query($con, $sql);
$compra = mysqli_fetch_assoc($consulta);
if (!$compra) {
    header('location:compras.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Detalhes da compra</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">
            <div class="page-header">
                <h1><i class="fa fa-shopping-cart"></i> Detalhes da compra #<?php echo $idcompra; ?></h1>
            </div>
            <?php
            if ($msgOk) {
                msgHtml($msgOk, 'success');
            }
            ?>
            <?php
            if ($msgAviso) {
                msgHtml($msgAviso, 'warning');
            }
            ?>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Produtos da compra</h3>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Qtd.</th>
                            <th>Produto</th>
                            <th>Preço unitário</th>
                            <th>Preço total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "Select
                        ei.idproduto,
                        p.produto,
                        ei.precopago,
                        ei.qtd
                        From compraitem ei
                        Inner Join produto p
                        On (p.idproduto = ei.idproduto)
                        Where (ei.idcompra = $idcompra)";
                        $consulta = mysqli_query($con, $sql);

                        $compraTotal = 0;

                        while ($produto = mysqli_fetch_assoc($consulta)) {
                            $total = $produto['qtd'] * $produto['precopago'];
                            $compraTotal += $total;
                            ?>
                            <tr>
                                <td><?php echo $produto['qtd']; ?></td>
                                <td><?php echo $produto['produto']; ?></td>
                                <td>R$ <?php echo number_format($produto['precopago'], 2, ',', '.'); ?></td>
                                <td>R$ <?php echo number_format($total, 2, ',', '.'); ?></td>
                            </tr>
<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th colspan="2">Total da compra</th>
                            <th>R$ <?php echo number_format($compraTotal, 2, ',', '.'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <form class="form-horizontal" method="post" action="compra-fechar.php">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fechamento da compra</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="fidcompra" class="col-sm-2 control-label">Código:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $idcompra; ?></p>
                            </div>

                            <label for="fdata" class="col-sm-2 control-label">Data:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo date('d/m/Y', strtotime($compra['data'])); ?></p>
                            </div>

                            <label for="ftotal" class="col-sm-2 control-label">Total:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static">R$ <?php echo number_format($compraTotal, 2, ',', '.'); ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fcliente" class="col-sm-2 control-label">Cliente:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['clienteNome']; ?></p>
                            </div>
                            <label for="fcpf" class="col-sm-2 control-label">Cpf/Cnpj:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['cpfcnpj']; ?></p>
                            </div>
                            <label for="frgie" class="col-sm-2 control-label">Rg/Ie:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['rgie']; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="finTipo" class="col-sm-2 control-label">Tipo:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"
                                       <?php if ($compra['inTipo'] == PESSOA_FISICA) { ?>
                                       <span class="label label-success">Fisica</span>
                                    <?php } else { ?>
                                        <span class="label label-warning">Juridica</span>
<?php } ?></p>
                            </div>
                            <label for="ftelefone" class="col-sm-2 control-label">Telefone:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['telefone']; ?></p>
                            </div>
                            <label for="fcelular" class="col-sm-2 control-label">Celular:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['celular']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fcelular" class="col-sm-2 control-label">CEP:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['cep']; ?></p>
                            </div>
                            <label for="fendereco" class="col-sm-2 control-label">Endereço:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['endereco']; ?></p>
                            </div>
                            <label for="fbairro" class="col-sm-2 control-label">Bairro:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['bairro']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fnumero" class="col-sm-2 control-label">Número:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['numero']; ?></p>
                            </div>
                            <label for="femail" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['email']; ?></p>
                            </div>
                            <label for="fvendedor" class="col-sm-2 control-label">Vendedor:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $compra['usuarioNome']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="panel-footer">
<!--                <a href="gerar-contas.php?idcompra=<?php echo $compra['idcompra']; ?>&acao=1" title="Gerar parcelas"><button type="submit" class="btn btn-info">Gerar parcelas</button></a>-->
                <a href="gerar-contas.php?idcompra=<?php echo $compra['idcompra']; ?>&idcliente=<?php echo $compra['idcliente']; ?>&acao=1" title="Pagar parcela"><button type="submit" class="btn btn-info">Gerar parcelas</button></a>
                <p class="form-control-static pull-right"><strong>TOTAL:</strong> R$ <?php echo number_format($compraTotal, 2, ',', '.'); ?></p>
            </div>
        </div>
        <script src="./js/gerar-conta-parcela.js"></script>
        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>

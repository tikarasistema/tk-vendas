<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

$sql = "Select idusuario, nome, nivelusu From usuario
  Where idusuario = idusuario
    And (situacao = '" . USUARIO_ATIVO . "')";
$consulta = mysqli_query($con, $sql);
$usuario = mysqli_fetch_assoc($consulta);

if ($usuario) {
    $_SESSION['libera'] = 1;
    $_SESSION['idusuario'] = $usuario['idusuario'];
    $_SESSION['nome'] = $usuario['nome'];
    $usuario = $_SESSION['nome'];
}

$descricao = '';
$saldo = '';
$data = '';
$num_parcela = 0;
$idcliente = 0;
//$status = PAGAR_PENDENTE;
//print_r($status);exit;

if ($_POST) {
    $descricao = $_POST['descricao'];
    $dtVencimento = $_POST['dtVencimento'];
    //print_r($dtVencimento);exit;
    //$data = date("Y-m-d", strtotime(str_replace('/', '-', $dtVencimento)));
    //$data= date_converter($dtVencimento);
    //print_r($data);exit;
    $saldo = $_POST['valor'];
    $num_parcela = $_POST['num_parcela'];
    $idcliente = (int) $_POST['cliente'];
    $data_operacao = date('Y-m-d');

    // $dtPagaRecebe = $_POST['dtPagaRecebe'];
    $data = date("Y-m-d", strtotime(str_replace('/', '-', $dtVencimento)));

    if ($idcliente <= 0) {
        $msg[] = 'Selecione um cliente';
    } else {
        $sql = "SELECT * FROM cliente WHERE idcliente = $idcliente";
        $consulta = mysqli_query($con, $sql);
        $cli2 = mysqli_fetch_assoc($consulta);
        if (!$cli2) {
            $msg[] = 'Este cliente não existe!';
        }
    }


    if ($descricao == '') {
        $msg[] = 'Informe a descrição';
    }
    if ($saldo == '') {
        $msg[] = 'Informe o saldo';
    }
    if ($data == '') {
        $msg[] = 'Informe a data de vencimento';
    }
    if ($num_parcela == '') {
        $msg[] = 'Informe o número da parcela';
    }
        if ($num_parcela <0) {
        $msg[] = 'O número da parcela não pode ser inferior a zero ';
    }
    if (!$msg) {
        //print_r($data);exit;
        $sql = "INSERT INTO contaspagar(descricao, valor, data_paga, data_operacao, num_parcela,entrada, usuario, idcliente) VALUES ('$descricao','$saldo','$data','$data_operacao','$num_parcela','" . SEM_ENTRADA . "' ,'$usuario','$idcliente')";
        $r = mysqli_query($con, $sql);
        if (!$r) {
            $msg[] = 'Erro para salvar';
            $msg[] = mysqli_error($con);
        } else {
            javascriptAlertFim('Registro foi salvo com sucesso !', 'contas-pagar.php');
        }
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cadastrar conta</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-money"></i> Cadastrar conta</h1>
                    </div>
                </div>
            </div>

            <?php
            if ($msg) {
                msgHtml($msg);
            }
            ?>

            <form class="row" role="form" method="post" action="contas-pagar-cadastrar.php">

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fcliente">Cliente</label>
                            <select class="form-control" id="fcliente" name="cliente">
                                <option value="0">Selecione um cliente</option>
                                <?php
                                $sql = "Select idcliente, nome From cliente Order by nome";
                                $q = mysqli_query($con, $sql);
                                while ($cliente = mysqli_fetch_assoc($q)) {
                                    ?>        
                                    <option value="<?php echo $cliente['idcliente']; ?>"
                                            <?php if ($idcliente == $cliente['idcliente']) { ?> selected <?php } ?>
                                            ><?php echo $cliente['nome']; ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fdescricao">Descrição</label>
                            <input type="text" class="form-control" id="fdescricao" name="descricao" placeholder="Descrição">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fvalor">Valor total R$</label>
                            <input type="text" pattern="[0-9]+$" class="form-control" id="fvalor" name="valor" placeholder="Ex: 1000.98">
                        </div>
                    </div>  
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fdtVencimento">Data de vencimento</label>
                            <input type="date" class="form-control" id="fdtVencimento"placeholder="Ex: dd/mm/YYYY" name="dtVencimento">
                        </div>
                    </div>
                </div>

                <div class="row">                   
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fnum_parcela">Número de parcela</label>
                            <input type="number" pattern="[0-9]+$" min="0" class="form-control" id="fnum_parcela" placeholder="No. de parcela" name="num_parcela">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </form>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
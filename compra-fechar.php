<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
// Pegar idvenda
if (!isset($_SESSION['idcompra'])) {
  header('location:compras.php');
  exit;
}
$idcompra = $_SESSION['idcompra'];
// Validar idvenda
$sql = "Select idcompra
        From compra
        Where
        (idcompra = $idcompra)
        And (situacao= " . COMPRA_ABERTA . ")";
$consulta = mysqli_query($con, $sql);
$compra = mysqli_fetch_assoc($consulta);
//print_r($compra);exit;
if (!$compra) {
  header('location:compras.php');
  exit;
}
// Fechar venda
$sql = "Update compra Set situacao=" . COMPRA_FECHADA
        . " Where (idcompra = $idcompra)";
mysqli_query($con, $sql);
unset($_SESSION['idcompra']);
// Redirecionar usuario para vendas.php
header('location:compra-detalhes.php?idcompra=' . $idcompra);

